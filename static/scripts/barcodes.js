"use strict";
// Oldschool JS! Woo!

document.addEventListener("DOMContentLoaded", () =>
{
    Array.prototype.forEach.call(document.querySelectorAll(".barcode-edit"), edit =>
    {
        let display = edit.closest(".barcode");
        let value = display.querySelector(".barcode-value");
        let row = edit.closest(".row");
        let parts = row.querySelectorAll(".barcode-submit");
        let form = Array.prototype.filter.call(parts, part => part.tagName === "FORM")[0];
        let input = row.querySelector("[name='barcode']");
        let cancel = row.querySelector(".barcode-cancel")

        let reset = () =>
        {
            Array.prototype.forEach.call(parts, part => part.style.display = "none");
            display.style.display = "";
        };
        cancel.addEventListener("click", reset);

        edit.style.display = "";
        edit.addEventListener("click", () =>
        {
            display.style.display = "none";
            input.value = value.textContent.trim();

            Array.prototype.forEach.call(parts, part => part.style.display = "");
            setTimeout(() => input.select(), 0);
        });

        form.addEventListener("submit", (event) =>
        {
            event.preventDefault();

            let data = new FormData(form);
            let request = new XMLHttpRequest();
            request.onreadystatechange = () =>
            {
                if(request.readyState === XMLHttpRequest.DONE && (request.status >= 200 && request.status < 400))
                {
                    value.textContent = input.value.trim();

                    if (input.value)
                    {
                        reset();
                    } else {
                        cancel.style.display = "none";
                    }
                }
            };
            request.open("POST", form.action);
            request.send(data);

            let next = row.closest("li").nextElementSibling;
            while (next != null)
            {
                let barcode = next.querySelector("[name='barcode']");
                if (barcode && barcode.offsetParent)
                {
                    barcode.focus();
                    break;
                }
                next = next.nextElementSibling;
            }

            return false;
        });
    });
});
