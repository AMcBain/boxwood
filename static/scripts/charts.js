const CHART_DATA = JSON.parse(document.getElementById('CHART_DATA').textContent);

const width = Math.min(window.innerWidth, 576);
const height = width * .4;
const margin = { top: 30, right: 0, bottom: 30, left: 40 };
let color = "#000";

// Largely borrowed from: https://observablehq.com/@d3/bar-chart
//                   and: https://observablehq.com/@d3/hierarchical-bar-chart
const checkoutChart = function (parent, back, text) {
  const breakdown = "Click on a day to view the hourly breakdown."
  
  const data = CHART_DATA.checkouts;
  data.forEach(day => day.count = day.hours.reduce((tally, hour) => tally + hour.count, 0));

  if (!data.length) {
    text.textContent = "No games played in this time range.";
    return;
  }

  const cmargin = Object.assign({}, margin);
  
  if (data.length > 8) {
    cmargin.bottom *= 2.5;
  }

  const x = d3.scaleBand()
    .domain(d3.range(data.length))
    .range([cmargin.left, width - cmargin.right])
    .padding(0.1);

  const y = d3.scaleLinear()
    .domain([0, d3.max(data, d => d.count)]).nice()
    .range([height - cmargin.bottom, cmargin.top]);

  const xAxisF = data => g => g
    .attr("transform", `translate(0,${height - cmargin.bottom})`)
    .call(d3.axisBottom(x).tickFormat(i => data[i].date || data[i].hour).tickSizeOuter(0));

  const yAxisF = data => g => g
    .attr("transform", `translate(${cmargin.left},0)`)
    .call(d3.axisLeft(y).ticks(7)) // FIXME Do properly; apparently getting only integers in the y-axis is difficult under d3v6+
    .call(g => g.select(".domain").remove())
    .call(g => g.append("text")
        .attr("x", -cmargin.left)
        .attr("y", 10)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .text(data.y));

  const xAxis = xAxisF(data);
  const yAxis = yAxisF(data);

  const svg = d3.create("svg")
      .attr("viewBox", [0, 0, width, height]);

  const bars = data => {
    const bars = svg.append("g")
        .attr("fill", color)
      .selectAll("rect")
      .data(data)
      .join("rect")
        .attr("class", "enter")
        .attr("x", (d, i) => x(i))
        .attr("y", d => y(d.count))
        .attr("height", d => y(0) - y(d.count))
        .attr("width", x.bandwidth());
    bars
      .append('title')
        .text(d => `${d.count} checkouts`);
    return bars;
  }

  const rotateLabels = g =>
    g.selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", ".15em")
      .attr("transform", "rotate(-65)");

  const unrotateLabels = g =>
    g.selectAll("text")
      .style("text-anchor", "middle")
      .attr("dx", "")
      .attr("dy", ".71em")
      .attr("transform", "");

  const zoomIn = d => {
    const data = d.hours;

    svg.selectAll(".enter")
      .remove();

    x.domain(d3.range(data.length));
    y.domain([0, d3.max(data, d => d.count)]);

    const xAxis = xAxisF(data);
    const yAxis = yAxisF(data);

    bars(data);

    svg.selectAll(".x-axis")
      .call(xAxis);

    unrotateLabels(g);

    svg.selectAll(".y-axis")
      .call(yAxis);

    back.classList.remove("d-none");
    text.textContent = `Checkouts on ${d.date} by hour.`;
  };

  bars(data)
    .on("click", (event, d) => zoomIn(d));

  const g = svg.append("g")
    .call(xAxis)
      .attr("class", "x-axis");

  if (data.length > 8) {
    rotateLabels(g);
  }

  svg.append("g")
    .call(yAxis)
      .attr("class", "y-axis");

  parent.appendChild(svg.node());
  parent.style.maxWidth = width + "px";

  back.addEventListener("click", () => {
    svg.selectAll(".enter")
      .remove();

    x.domain(d3.range(data.length));
    y.domain([0, d3.max(data, d => d.count)]).nice();

    bars(data)
      .on("click", (event, d) => zoomIn(d));

    svg.selectAll(".x-axis")
      .call(xAxis);

    if (data.length > 8) {
      rotateLabels(g);
    }


    svg.selectAll(".y-axis")
      .call(yAxis);

    back.classList.add("d-none");
    text.textContent = breakdown;
  });

  text.textContent = breakdown;
};

const filterForm = function (startDate, endDate) {
  const change = () => {
    // If browsers supported "iso8601" as a valid locale for Intl.DateTimeFormat() we wouldn't even need Luxon here. >:(
    // It's a sledge hammer to put in a finishing nail.
    const futureEnd = luxon.DateTime.fromISO(startDate.value).plus(luxon.Duration.fromISO("P14D"));
    const today = luxon.DateTime.now().startOf('day');
    const max = futureEnd > today ? today : futureEnd;

    if (max < luxon.DateTime.fromISO(endDate.value)) {
      endDate.value = max.toFormat("yyyy-MM-dd");
    }
    endDate.min = startDate.value;
    endDate.max = max.toFormat("yyyy-MM-dd");
  };
  startDate.addEventListener("input", change);
  change();
};

document.addEventListener("DOMContentLoaded", () => {
  const span = document.createElement("span");
  span.classList.add("bg-info");
  document.body.appendChild(span);
  color = window.getComputedStyle(span).backgroundColor;
  document.body.removeChild(span);

  checkoutChart(
    document.getElementById("checkout_chart"),
    document.getElementById("checkout_chart_back"),
    document.getElementById("checkout_chart_text")
  );

  filterForm(
    document.getElementById("id_start"),
    document.getElementById("id_end")
   );
});
