# Boxwood

This is an app for manging a board game library based on other people's collections. It also has the ability to do check outs for game days and other events.

## Installation and Setup

### Pre-reqs

- Python 3.8.x+ (probably works with at least 3.7.x+)
- `pipenv` (though having `pip` and `venv`/`virtualenv` probably works)

### Steps

These can be done from the virtualenv shell by running `pipenv shell` first. If you do then don't forget to remove it from each command.
If you're on Windows using Git Bash you may find it nicer to use `winpty python <args here...>`.

1. Clone the project.
2. Run `pipenv install` in the checkout directory.
3. Copy `boxwood/settings.dev.py` and rename it to `boxwood/settings.py`

    This is to avoid checking in secret keys to the repo and so we can later have a proper `settings.prod.py` with different stuff in it.

4. Run `python -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'`
5. Copy the output into the empty string assigned to `SECRET_KEY` in `boxwood/settings.py`.
6. Log in to Board Game Atlas and visit `https://www.boardgameatlas.com/api/docs/apps`
7. Create a new app if you don't already have one for this project
8. If creating a new one, set the OAuth2 return/redirect URL to `http://localhost:8000/complete/boardgameatlas/` (for dev purposes)

    You should probably create a new app entry for the live site, so you can keep this one for testing.
    The live site would use `https://(yourdomainhere)/complete/boardgameatlas/` instead of the above.

9. Edit `boxwood/settings.py` and fill in the `SOCIAL_AUTH_BOARDGAMEATLAS_KEY` (client id) and `SOCIAL_AUTH_BOARDGAMEATLAS_SECRET` values.
10. While in `boxwood/settings.py` change `ORG_*` variables to what is correct for your organization.
11. Run `pipenv python ./manage.py migrate`
12. Run `pipenv python ./manage.py createcachetable`
13. Run `pipenv python ./manage.py createsuperuser` (optional; see below)
14. Run `python ./manage.py loaddata default`
15. Run `python ./manage.py updatetags
16. Run `pipenv python ./manage.py runserver`
17. Visit http://127.0.0.1:8000/
18. Click `Link with Board Game Atlas` and log in there.
19. If VERIFY_NEW_USERS is enabled, you'll also have to fill out and submit the verification form before the next step.
20. Open a new private window and visit http://127.0.0.1:8000/admin
21. Log in as the superuser created in step 11.
22. Under `Users` on the left, find and edit your user.
23. Check the `Staff status` and `Superuser status` boxes.
24. Save your user and close the window. Your primary account now has superpowers and can see the same admin area.
25. If VERIFY_NEW_USERS is enabled, you'll also want to visit http://127.0.0.1:8000/users/requests and approve your user's request.

Step 12 is only needed if using the default development cache which stores itself in the database. In memory caches or other setups obviously don't need table setup.

Step 13 isn't strictly necessary. An alternative method would be to log in to the app via Board Game Atlas as the first user.
Afterwards you can directly change the database to set `is_superuser` and `is_staff` to `1` (number). You will now have access to the admin area.
If you do create the superuser, you can use that user to log into the admin area and set the same flags on your other account, for ease of development.

If you plan to contribute to development, then see the further sections below for more help and information.

### Running the App

1. `pipenv python ./manage.py runserver`
2. Open http://127.0.0.1:8000/ in your browser of choice.
3. Click the button `Log In with Board Game Atlas`.

## Development

This area covers general workings of how things are set up and what to follow, for consistency.
If you don't like how something is done and you have a good idea on how to make it better, send it across before you do it and we can discuss it.
Otherwise just follow along with what's already there.

### Adding New Dependencies

If for some reason you have to add a new dependency, this is the preferred way to do it so we can keep track of it.

1. `pipenv install [package name here]`

It takes the same argument format for specifying name and version numbers as `pip`.

### Project Layout

The project layout may not be completely "standard" but it does follow the required names for models, fixtures, templatetags, and management.
Static and template directories also follow the general convention.

### Decorators

This project uses decorators a lot for setup and avoiding duplication of code. "Magic," if you prefer to call it that.
However they all are defined within the app so they're almost all easily located and read.
All of them should be straight-forward. None have "hacks" and none do any crazy patching of libraries or Python itself.

### Models

Models are in the standard module, but use a folder instead of a single `models.py` to help organize things into multiple files without cluttering the project root.
All models tagged with `@model` will be automatically registered with django so there's no manual master list of models.

```python
from django.db import models
from boxwood import model

@model
class Thing(models.Model):
    pass
```
It's a little confusing, out of necessity, but the `@model` decorator is located in `boxwood/__init__.py` to avoid problems with accidentally writing circular imports.

### Routing

These are stored in the `controllers` folder. As with models, routes are also automatically registered by way of decorators.
This one is more of an opinion, since it's not necessarily any easier and it does scatter the route info instead of keeping it together.

`@route`s take the same arguments as would be passed to django's `path()` or `re_path()`. The former is the default, but route paths starting with `^` or ending in `$` will use the latter.
Multiple route decorators can be applied to the same function. Decorators should always be listed first if there's more than one decorator, since decorators are effectively applied in reverse order.

```python
@route('')
def index(request):
  pass
```
Due to how this is handled, the dev server process will need to be killed and restarted to pick up any new files under the controllers folder.
New routes within existing files will get picked up just fine.

The name of the route, for purposes of reverse routing (via `{% url ... %}` in templates), is taken from the function name.
To pick something different or to differentiate two routes on a single function, you can provide a name argument:

```python
@route('/foo')
@route('/bar', name='bar')
def foo(request):
  pass
```

The full name of each route is `boxwood.controllers.<module>.<name>`, so a route called `lists` in `controllers/users.py` would be generated like so:
```django
<a href="{% url 'boxwood.controllers.user.lists' ... %}">Linky</a>
```
All links to pages should be done with `{% url ... %}` when possible to avoid link rot if the controllers change. (Also saves us having to update every link if we do.)
If GET parameters are needed they can be tacked on to the end after the url tag.

```django
<a href="{% url 'boxwood.controllers.user.lists' ... %}?page=2">Linky with params</a>
```

One last thing to note: if a controller function is named the same as the module it resides in, the controller function is ommitted.
Thus `def users(request):` in `controllers/users.py` has a full name of `boxwood.controllers.users` instead of `users.users`.

#### Other Controller Decorators

Decorators such as `@render_html` are used to generate a standard render response by passing the object returned as the context.
These decorators should come just after all `@route` decorators. They aren't required, so standard controller functions are still possible.
They will also pass through any response that is already an HttpResponse if you need something different in certain cases.

Controllers decorated this way expect their templates to be at `templates/views/<module>/<func_name>.<ext>`
where the module is the Python module (file it is defined in), func name is the name of the function, and the extension is indicated by the renderer (`html` here).

```python
@route('/foo')
@render_html
def foo(request):
  return {
    'thing': 42
  }
```

Controllers decorated with a renderer can have `@page` applied to validate `page` and `count` parameters as well as automatically page a QuerySet value returned from the function.
If no argument is supplied to `@page` then the decorator will only validate and ensure the param `page` is a number >= 1 and 10 >= `count` <= 100, else 50 if not supplied or not a number.

An example of `@page` with just validation applied and manual paging is below.
This is useful if paging is more complicated than just slicing up a single item's return set.
```python
@route('/thing')
@render_html
@paged
def thing(request, page=1, count=50):
    page -= 1
    start = page * count
    return {
        'list': Thing.objects.all()[start:start + count]
    }
```

Adding an argment causes that argument to be paged based on the `page` and `count` params. 
```python
@route('/thing')
@render_html
@paged('list')
def thing(request):
    return {
        'list': Thing.objects.all()
    }
```

Finally, the `@verified` decorator ensures only verified users or unverified users with complete verification requests can visit this URL.
Unverified users with incomplete requests (missing the request context info) will automatically be redirected to a page where they can finish their verification request.
This decorator can be placed anywhere after route, but for performance it is probably best to ensure it is at the bottom of all decorators.
```python
@route('/thing')
@render_html
@paged('list')
@verfied
def thing(request):
    return {
        'list': Thing.objects.all()
    }
```
This should not be applied to POST requests to avoid any potential strangeness or security issues.

### Templates

Partly covered in other sections, there are a few useful things to know.

Inside the templates folder, there are several subfolders.
The `widgets` folder is for templates that override built in templates used by django forms' widgets.
The `tags` folder is for included templates, which is covered below. The other folders should be self-explanatory.
All base templates, which others extend, live at the root of the templates folder.

Firstly, if you see `{% load ... %}` in a template, that is for external libraries and in a couple cases, our own code.
So if you don't recognize it, check `boxwood/templatetags` to find ours. The loaded name is the same as the file name.
Similarly, if you see a tag used in a template you don't recognize and doesn't seem built in, look for a load tag near the top of the file.

Templates that don't need full Python code backing are loaded with `{% include 'tags/...' ... %}` and live at the specified location.
By default it will start looking for these at the root of the templates folder. Ours, by convention, live in `template/tags`.

Included template "tags" are just regular django templates with no special exceptions.
They can be passed any named parameters (turned into variables inside the template).
Currently we just have a few, but the most useful ones will be the pagination, games, and errors tags, just to avoid repeating ourselves.

#### Blocks

django defines sections used by templates as blocks, and our base templates define a few of our own.

**Title**
```django
{% block title %}
  Stuff here
{% endblock %}
```
This is used to define part of the page title relating to the current page. Some templates, like `subpage.html`, also use this to define a heading for the page.
Therefore it's best not to put more than just text in here at the moment.

**Nav**
```django
{% block nav %}
  All page/site-level navigation goes in here. You will rarely need to override this in full. See the next block.
{% endblock %}
```

**Breadcrumbs**
```django
{% block breadcrumb %}
  {{ block.super }}
  <li class="breadcrumb-item active" aria-current="page">
    {{ username }}
  </li>
{% endblock %}
```
This is where all pages extending either base template should put the current navigation for the page.
The call to super ensures the default non-active breadcrumb for subpages is picked up when extending `subpage.html`.
All pages extending `base.html` should likely not call super so they can override the base breadcrumb and provide one that isn't active (per ARIA).

**Header**
```django
{% block header %}
  {{ block.super }}
  Stuff here.
{% endblock %}
```
Anything that goes before content should go in here. Heading elements, etc.
Currently there's nothing special about this, but in the future it might be turned into a proper `<header>` tag.

**Content**
```django
{% block content %}
  {{ block.super }}
  Main content!
{% endblock %}
```
Calling super isn't strictly necessary here when extending `base.html` but it is useful when extending other templates to pick up potential content they've defined.
