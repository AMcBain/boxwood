from datetime import date, timedelta
from django import forms

class FiltersForm(forms.Form):
    
    # Allow longer? Depends on at what point their service barfs.
    filter_criteria = forms.CharField(
      label = 'Filter By',
      max_length = 128,
      required = False,
      help_text = 'Filter by name, year, category, or mechanic.',
    )
    
    filter_max_players = forms.TypedChoiceField(
        label = 'Max Players',
        choices = [
          (0, 'Any'),
          (-1, 'Solo'),
          (1, '1'),
          (2, '2'),
          (3, '3'),
          (4, '4'),
          (5, '5'),
          (6, '6+'),
        ],
        empty_value = 0,
        coerce = int,
        required = False,
    )
    
    filter_max_play_time = forms.TypedChoiceField(
        label = 'Max Play Time',
        choices = [
          (0, 'Any'),
          (15, '15m'),
          (30, '30m'),
          (45, '45m'),
          (60, '1h'),
          (90, '1.5h'),
          (120, '2h'),
          (121, '> 2h')
        ],
        empty_value = 0,
        coerce = int,
        required = False,
    )

class RealDateInput(forms.DateInput):
    input_type = 'date'
    
    def __init__(self, min_date=None, max_date=None):
        if (min_date or max_date):
          super().__init__(attrs={
              'min': min_date,
              'max': max_date,
          })
        else:
          super().__init__(attrs={})

class DateRangeForm(forms.Form):
    
    start = forms.DateField(
        label = 'Start Date',
        input_formats = ['%Y-%m-%d'],
        initial = lambda: date.today() - timedelta(days=7),
        help_text = 'Inclusive',
        widget = RealDateInput(),
    )

    end = forms.DateField(
        label = 'End Date',
        input_formats = ['%Y-%m-%d'],
        initial = date.today,
        help_text = 'Inclusive',
        widget = RealDateInput(
            min_date = (date.today() - timedelta(days=7)).isoformat(),
            max_date = date.today().isoformat()
        ),
    )
