from django import forms

class LibraryAddGameForm(forms.Form):
    
    # Allow longer? Depends on at what point their service barfs.
    criteria = forms.CharField(
      label = 'Search',
      max_length = 128,
      help_text = 'Look up games by name, year, category, or mechanic.'
    )
