# Generated by Django 3.1.6 on 2021-02-14 10:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('boxwood', '0004_add_user_profile_and_roles'),
    ]

    operations = [
        migrations.AlterField(
            model_name='librarygame',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxwood.listgame'),
        ),
        migrations.AlterField(
            model_name='librarygame',
            name='library',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxwood.library'),
        ),
        migrations.AlterField(
            model_name='listgame',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='boxwood.game'),
        ),
    ]
