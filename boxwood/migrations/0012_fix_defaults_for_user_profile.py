# Generated by Django 3.1.6 on 2021-02-15 06:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boxwood', '0011_fix_tag_default_rank'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='twitter_handle',
            field=models.CharField(blank=True, default='', help_text='(Optional) Your Twitter handle.', max_length=15, verbose_name='Twitter'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='about',
            field=models.CharField(blank=True, default='', help_text='(Optional) Max 500 characters. Anything extra you want to include on your user page.', max_length=500),
        ),
        migrations.AlterField(
            model_name='profile',
            name='bga_username',
            field=models.CharField(blank=True, default='', help_text='(Optional) Your Board Game Arena username.', max_length=20, verbose_name='BGA Username'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='bgg_username',
            field=models.CharField(blank=True, default='', help_text='(Optional) Your Board Game Geek username.', max_length=20, verbose_name='BGG Username'),
        ),
    ]
