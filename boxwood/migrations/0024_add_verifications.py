# Generated by Django 3.1.6 on 2021-03-06 04:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
        ('boxwood', '0023_add_board_member_flag'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserVerificationRequest',
            fields=[
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, primary_key=True, related_name='verification_request', serialize=False, to='auth.user')),
                ('staff_context', models.CharField(blank=True, default='', help_text="Required. Max 500 characters. This is only to let the staff know who you are and your relation to Tri-City Area Gaming so they can approval purposes. You'll be able to set your public profile later.", max_length=500)),
            ],
        ),
        migrations.AddField(
            model_name='profile',
            name='verified',
            field=models.BooleanField(default=False, help_text='For staff and admins only, to indicate who can contribute suggestions or have games included in libraries.'),
        ),
    ]
