from django.core.management.base import BaseCommand, CommandError
from boxwood.models.tags import Mechanic, Category
from boxwood.api import backend

class Command(BaseCommand):
    help = 'Updates the categories and mechanics in the db'

    def handle(self, *args, **options):
        
        mechanics = set(Mechanic.objects.all().values_list('id', flat=True))
        mechanics_added = 0
        mechanics_updated = 0
        
        for mechanic in backend.fetch_mechanics():
            if mechanic.id in mechanics:
                mechanics.remove(mechanic.id)
                mechanic.save(update_fields=['name'])
                mechanics_updated += 1
            else:
                mechanics_added += 1
                mechanic.save()
        
        mechanics_removed = len(mechanics)
        Mechanic.objects.filter(id__in=mechanics).delete()
        
        if mechanics_added:
            self.stdout.write(self.style.SUCCESS('%s mechanics added' % str(mechanics_added)))
        
        if mechanics_updated:
            self.stdout.write(self.style.SUCCESS('%s mechanics updated' % str(mechanics_updated)))
        
        if mechanics_removed:
            self.stdout.write(self.style.SUCCESS('%s mechanics removed' % str(mechanics_removed)))
        
        categories = set(Category.objects.all().values_list('id', flat=True))
        categories_added = 0
        categories_updated = 0
        
        for category in backend.fetch_categories():
            if category.id in categories:
                categories.remove(category.id)
                category.save(update_fields=['name'])
                categories_updated += 1
            else:
                categories_added += 1
                category.save()
        
        categories_removed = len(categories)
        Category.objects.filter(id__in=categories).delete()
        
        if categories_added:
            self.stdout.write(self.style.SUCCESS('%s categories added' % str(categories_added)))
        
        if categories_updated:
            self.stdout.write(self.style.SUCCESS('%s categories updated' % str(categories_updated)))
        
        if categories_removed:
            self.stdout.write(self.style.SUCCESS('%s categories removed' % str(categories_removed)))
