from collections.abc import Callable
from functools import wraps
from math import ceil
from urllib.parse import quote
from django.contrib.auth.models import AnonymousUser
from django.views import defaults
from django.http import HttpResponseNotAllowed
from django.http.response import HttpResponseBase
from django.shortcuts import render, redirect
from django.urls import reverse
from django.db.models import Q
from boxwood.forms.filters import FiltersForm
from boxwood.api.base import parse_criteria

# All this just to change the location of the error templates. Uuuuugh.
# Also this is the problem with django, the default is just to have everything all in the same folder because structure is apparently evil.
ERROR_404_TEMPLATE_NAME = 'errors/404.html'
ERROR_403_TEMPLATE_NAME = 'errors/403.html'
ERROR_400_TEMPLATE_NAME = 'errors/400.html'
ERROR_500_TEMPLATE_NAME = 'errors/500.html'


def bad_request(request, exception, template_name=ERROR_400_TEMPLATE_NAME):
    return defaults.bad_request(request, exception, template_name)


def permission_denied(request, exception, template_name=ERROR_403_TEMPLATE_NAME):
    return defaults.permission_denied(request, exception, template_name)


def page_not_found(request, exception, template_name=ERROR_404_TEMPLATE_NAME):
    return defaults.page_not_found(request, exception, template_name)


def server_error(request, template_name=ERROR_500_TEMPLATE_NAME):
    return defaults.server_error(request, template_name)


# Decorators intended to aid in writing easier controller functions.
# Suitable for even controller functions that return HttpResponse-types.


# Returns a decorator function.
def render_format(ext):
    # This is called with a function if no args are given to the decorator.
    # If an arg is given, no function is passed and this is given the user-declared arg.
    # Principle of least surprise my ass.
    def renderer(func=None, template=None):
        def handler(func):
            # We want to keep all args to the decorator as someone may have configured the path to take an extra dictionary.
            @wraps(func)
            def renderer(request, *args, **kwargs):
                data = func(request, *args, **kwargs)
                
                # Make sure we preserve the behavior where someone can explicitly return an HttpResponse for non-standard returns.
                # We don't have to worry about things like Http404 here since those are raised as errors instead.
                if isinstance(data, HttpResponseBase):
                    return data
                
                file = template if template else func.__name__
                module = func.__module__
                
                if module.startswith('boxwood.controllers.'):
                    module = module.replace('boxwood.controllers.', '')
                
                return render(request, 'views/' + module + '/' + file + '.' + ext, data)
            
            return renderer
        # So if we get a function, deal with it immediately.
        if isinstance(func, Callable):
            return handler(func)
        # Otherwise wait until we get called to get the function instance.
        return handler
    return renderer

# This could just be shortened to 'html' but then it'd be harder to know if it was decorating for incoming or outgoing purposes.
# Should always be just after @route so it captures all output from other result-affecting decorators.
render_html = render_format('html')

# Ensure all paged routes get a page variable that is already pre-validated to be an integer >= 0
def paged(arg=None, *args, **kwargs):
    """
    Decorator to page a given variable or at least validate the page and count params from a request.
    
    # Manual paging, if needing to do something more complicated.
    @route('/thing')
    @render_html
    @paged
    def thing(request, page=1, count=50):
        page -= 1
        start = page * count
        return {
            'list': Thing.objects.all()[start:start + count]
        }
    
    # Autopaging if it's simply just returning a subset of a single result item.
    @route('/thing')
    @render_html
    @paged('list')
    def thing(request):
        return {
            'list': Thing.objects.all()
        }
    """
    def handler(func):
        @wraps(func)
        def paged(request, *args, **kwargs):
            params = request.GET if request.method == 'GET' else request.POST
            kwargs['page'] = pg = max(1, int(params['page'])) if 'page' in params and params['page'].isdecimal() else 1
            # Until we reveal it on the front-end, this is a secret variable. :)
            kwargs['count'] = count = max(10, min(int(params['count']), 100)) if 'count' in params and params['count'].isdecimal() else 50
            try:
                result = func(request, *args, **kwargs)
                if isinstance(result, HttpResponseBase):
                    return result
            except TypeError as err:
                if str(err).find('got an unexpected keyword argument \'page\'') == -1:
                    raise err
                # The route handler didn't declare the page and count variables.
                # This is easier than trying to inspect it to determine if they declared them.
                del kwargs['page']
                del kwargs['count']
                result = func(request, *args, **kwargs)
                if isinstance(result, HttpResponseBase):
                    return result
            if arg and result[arg]:
                # This does hit the DB again, but if the list is big we avoid loading all of it right now.
                result[arg + '_total'] = result[arg].count()
                result[arg + '_pages'] = ceil(result[arg + '_total'] / count)
                result[arg] = page(result[arg], pg, count)
            result['page'] = pg
            result['count'] = count
            return result
        return paged
    # See renderer (above) on this bit.
    if isinstance(arg, Callable):
        return handler(arg)
    return handler

def page(result, page, count):
    page -= 1
    start = page * count
    return result[start:start + count]

# Handle filters forms (criteria text, max players, max play time)
# The prefix argument is for helping this know at what level we're at (librarygame, listgame, etc.)
# Ex: @filtered('games', 'game__')
def filtered(arg=None, prefix=''):
    def handler(func):
        @wraps(func)
        def filtered(request, *args, **kwargs):
            result = func(request, *args, **kwargs)
            
            if isinstance(result, HttpResponseBase):
                return result
            
            # Up to the templates to ensure this is really used.
            form = FiltersForm(request.GET)
            result['filters_form'] = form
            
            # It should always be valid, but whatever. All fields, as of typing this, are optional. :)
            if arg in result and form.is_valid():
                criteria = parse_criteria(form.cleaned_data['filter_criteria'])
                filters = dict()
                
                if 'name' in criteria:
                    filters[prefix + 'name__icontains'] = criteria['name']
                
                if 'gt_year_published' in criteria:
                    filters[prefix + 'published__gt'] = criteria['gt_year_published']
                
                if 'lt_year_published' in criteria:
                    filters[prefix + 'published__lt'] = criteria['lt_year_published']
                
                if 'filter_max_players' in request.GET:
                    max_players = int(request.GET['filter_max_players'])
                    
                    if max_players >= 6:
                        filters[prefix + 'max_players__gt'] = 5
                    elif max_players > 0:
                        filters[prefix + 'max_players'] = max_players
                    elif max_players < 0:
                        # Negative numbers for weird stuff, like solo play (1 player).
                        filters[prefix + 'min_players__gte'] = max_players
                
                if 'filter_max_play_time' in request.GET:
                    max_play_time = int(request.GET['filter_max_play_time'])
                    
                    if max_play_time > 120:
                        filters[prefix + 'max_playtime__gt'] = 120
                    elif max_play_time > 0:
                        filters[prefix + 'max_playtime__lte'] = max_play_time
                
                if 'mechanics' in criteria:
                    filters[prefix + 'mechanics__id__in'] = criteria['mechanics']
                
                if 'categories' in criteria:
                    filters[prefix + 'categories__id__in'] = criteria['categories']
                
                if 'barcodes' in criteria:
                    if prefix.find('listgame') >= 0:
                        # (original criteria) and (library barcodes or listgame barcodes)
                        filters = Q(**filters)
                        filters &= Q(barcode__in=criteria['barcodes']) | Q(listgame__barcode__in=criteria['barcodes'])
                    elif prefix.find('game') >= 0:
                        # The game table doesn't have a barcode field.
                        filters['barcode__in'] = criteria['barcodes']
                    else:
                        filters[prefix + 'barcode__in'] = criteria['barcodes']
                
                if len(filters):
                    if isinstance(filters, Q):
                        result[arg] = result[arg].filter(filters)
                    else:
                        result[arg] = result[arg].filter(**filters)
                    result[arg] = result[arg].distinct()
                    result['has_filters'] = True
                
                return result
        
        return filtered
    # Shouldn't really be calling it without arguments, but let's handle it anyway.
    if isinstance(arg, Callable):
        return handler(arg)
    return handler

# This is to ensure non-verified users can't skip the please-fill-out-your-verification-request-form page.
# Kinda bad that we have to tag ALL get requests with it? Yeah. :( Maybe there's a better way to intercept.
def verified(func):
    @wraps(func)
    def verified(request, *args, **kwargs):
        # Not a good idea to handle on POST requests. Send those on, just in case a route was wrongly tagged.
        if request.method == 'POST' or request.user.is_anonymous or request.user.profile.is_verification_ready():
            return func(request, *args, **kwargs)
        url = reverse('boxwood.controllers.user.ready_verification', args=[request.user.username])
        return redirect(url + ('?' if url.find('?') == -1 else '&') + 'next=' + quote(request.get_full_path()))
    return verified
