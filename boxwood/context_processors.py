from django.conf import settings

def app_details(request):
    return {
        'APP_NAME': settings.APP_NAME,
        'POWERED_BY': settings.POWERED_BY,
        'ORG_NAME': settings.ORG_NAME,
        'ORG_NAME_SHORT': settings.ORG_NAME_SHORT,
    }
