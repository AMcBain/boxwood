from importlib import import_module

def __getattr__(name):
    if name == 'backend':
        from django.conf import settings
        from .base import BoardGameApiBase
        
        dot = settings.BOARD_GAME_API_BACKEND.rfind('.')
        api_module = import_module(settings.BOARD_GAME_API_BACKEND[0:dot])
        api = getattr(api_module, settings.BOARD_GAME_API_BACKEND[dot + 1:])()
        
        # Having a raise here won't cause any issues, noooo...
        if not isinstance(api, BoardGameApiBase):
            raise TypeError('BOARD_GAME_API_BACKEND is not a type of BoardGameApiBase')
        
        items = {'backend': api}
        __all__ = list(items.keys())
        globals().update(items)
        
        return api
    raise AttributeError(f"module '{__name__}' has no attribute '{name}'")
