import requests
from django.conf import settings
from .base import BoardGameApiBase, parse_criteria
from boxwood.models import Category, Mechanic, Game, ListGame, AtlasList

API_BASE = 'https://api.boardgameatlas.com/'
API_PATH = API_BASE + 'api/'

# Calls for APIs supported by the Atlas

class BoardGameAtlasApi(BoardGameApiBase):
    
    def fetch_api_result(self, path, params=None):
        if params:
            params['client_id'] = settings.SOCIAL_AUTH_BOARDGAMEATLAS_KEY
        else:
            params = { 'client_id': settings.SOCIAL_AUTH_BOARDGAMEATLAS_KEY }
        
        req = requests.get(API_PATH + path, params=params)
        
        if req.status_code == requests.codes.ok:
            return req.json()
    
    def fetch_categories(self):
        return map(lambda category: Category.create(category), self.fetch_api_result('game/categories')['categories'])
    
    def fetch_mechanics(self):
        return map(lambda mechanic: Mechanic.create(mechanic), self.fetch_api_result('game/mechanics')['mechanics'])
    
    def fetch_list(self, alist, skip=0):
        result = self.fetch_api_result('search', {
            'list_id': alist.id,
            'skip': skip,
        })
        return (
            map(lambda game: ListGame.create(alist, game), result['games']),
            result['count'],
        )
    
    def fetch_user_lists(self, user):
        return map(lambda entry: AtlasList.create(user, entry), self.fetch_api_result('lists', { 'username': user.username })['lists'])
    
    def find_games(self, criteria):
        params = {
        'name': '',
          'fuzzy_match': True,
          'limit': 10,
          'order_by': 'name,year_published,popularity',
          'ascending': True
        }
        
        parse_criteria(criteria, params, barcodes=False)
        
        if 'mechanics' in params:
            params['mechanics'] = ','.join(params['mechanics'])
        
        if 'categories' in params:
            params['categories'] = ','.join(params['categories'])
        
        result = self.fetch_api_result('search', params)
        return (
            filter(
                lambda game: game.type == "game",
                map(lambda entry: Game.create(entry), result['games']),
            ),
            int(result['count']),
        )
