import re
from abc import ABCMeta, abstractmethod
from boxwood.models import Category, Mechanic

# All the functions a given backend is required to have.
class BoardGameApiBase(metaclass=ABCMeta):
    
    @abstractmethod
    def fetch_categories(self):
        return []
    
    @abstractmethod
    def fetch_mechanics(self):
        return []
    
    @abstractmethod
    def fetch_list(self, alist, skip=0):
        return ([], 0,)
    
    @abstractmethod
    def fetch_user_lists(self, user):
        return []
    
    @abstractmethod
    def find_games(self, criteria):
        return ([], 0,)

def parse_criteria(text, criteria=None, barcodes=True):
    # Cannot put in args as default value; mutable default values get shared across calls in Python.
    if not criteria:
        criteria = dict()
    
    parts = filter(lambda part: part.strip(), text.split(' '))
    for part in parts:
        
        if barcodes and re.fullmatch(r'\d{12}\d?', part):
            if not 'barcodes' in criteria:
                criteria['barcodes'] = set()
            criteria['barcodes'].add(part)
        elif re.fullmatch(r'\d{4}', part):
            criteria['gt_year_published'] = int(part) - 1
            criteria['lt_year_published'] = int(part) + 1
        elif re.fullmatch(r'\d{4}-\d{4}', part):
            years = part.split('-')
            criteria['gt_year_published'] = int(years[0]) - 1
            criteria['lt_year_published'] = int(years[1]) + 1
        else:
            # Search for mechanics and categories first
            mechanics = Mechanic.objects.filter(name__icontains=part)
            categories = Category.objects.filter(name__icontains=part)
            
            # > 3 is to prevent matching "a", "an", and "the", etc.
            if len(part) > 3 and (len(mechanics) or len(categories)):
                
                if not 'mechanics' in criteria and mechanics:
                    criteria['mechanics'] = set()
                
                for mechanic in mechanics:
                    criteria['mechanics'].add(mechanic.id)
                
                if not 'categories' in criteria and categories:
                    criteria['categories'] = set()
                
                for category in categories:
                    criteria['categories'].add(category.id)
            else:
                if not 'name' in criteria:
                    criteria['name'] = ''
                criteria['name'] += ' ' + part if criteria['name'] else part
    
    return criteria

