from django import template

register = template.Library()

@register.filter(name='range')
def do_range(value):
    return range(0, value)
