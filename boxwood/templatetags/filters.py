from urllib.parse import quote
from django import template
from django.utils.datastructures import MultiValueDictKeyError
from boxwood.forms.filters import FiltersForm

register = template.Library()

@register.simple_tag
def filter_params(request, filters_form):
    params = ''
    if filters_form:
        for field_name in filters_form.fields:
            if field_name in request.GET:
                try:
                    params += '&' + field_name + '=' + quote(request.GET[field_name])
                except MultiValueDictKeyError:
                    # We should do something, but this seems to come up when there's no value.
                    # If there's no value, then no point in keeping it.
                    pass
    return params
