from django import template
from uuid import uuid4

register = template.Library()

@register.simple_tag
def uuid():
    return uuid4()
