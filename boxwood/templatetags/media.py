from django import template
from django.conf import settings
from django.db.models.fields.files import FieldFile

register = template.Library()

# I disliked the official solution for getting the MEDIA_URL:
# https://docs.djangoproject.com/en/3.1/ref/templates/builtins/#get-media-prefix
# They could have made this tag just like they did for {% static %} and since we would have to do {% load static %} anyway...
@register.simple_tag
def media(file):
    if isinstance(file, FieldFile):
        return file.url
    return settings.MEDIA_URL + file
