from sys import maxsize
from time import sleep, monotonic_ns
from django.contrib.auth.models import User
from boxwood.jobs.job import SyncingJob
from boxwood.models.lists import AtlasList, ListGame
from boxwood.api import backend

# Remove most common lists created from a BGG import that indicate the person doesn't actually have the game.
BLOCKLIST = set(['wishlist', 'previously owned', 'prev. owned', 'want to play', 'want to buy', 'preordered', 'pre-ordered'])

class UpdateUserListsJob(SyncingJob):
    
    def __init__(self, user):
        super().__init__(user.profile)
        self.user = user
    
    def do_sync(user):
        # max 60 requests per minute; we want to be a good API user.
        rate = 3000
        last = monotonic_ns() / 1000000
        
        # We need to remove all the ones that didn't update since they're gone on the other side.
        deleted = set(AtlasList.objects.filter(user=user).values_list('id', flat=True))
        
        lists = list(filter(lambda entry: not entry.name.lower() in BLOCKLIST, backend.fetch_user_lists(user)))
        # TODO Yes there are faster "bulk" ways to do this with temporary tables perhaps. Fix it later?
        for alist in lists:
            if alist.id in deleted:
                deleted.remove(alist.id)
                # Don't overwrite the internal fields for our use only.
                alist.refresh_from_db(fields=['lendable', 'is_syncing', 'last_synced'])
            alist.save()
            # To prevent double-calling, like if someone hits a profile and then scrolls down and enters a list we haven't gotten to yet.
            if alist.is_ok_to_sync():
                # So that we have data by the time the user goes into a list, hopefully.
                UpdateAtlasListJob(alist).now()
            # Rate limit ourselves
            now = monotonic_ns() / 1000000
            sleep((rate - (now - last)) / 1000)
            last = now
        
        AtlasList.objects.filter(id__in=deleted).delete()

    def now(self):
        # django models can be pickled! https://docs.djangoproject.com/en/3.1/ref/models/instances/#pickling-objects
        # Just so long as we don't cross django versions, we're cool. This won't happen in our usecase.
        # Also this is only even close to an issue if we switch to processes from threading, etc.
        return super().now(self.user)

class UpdateAtlasListJob(SyncingJob):

    def __init__(self, alist, concurrent_jobs=0):
        super().__init__(alist)
        self.alist = alist
        self.concurrent_jobs = concurrent_jobs
    
    def do_sync(alist, concurrent_jobs):
        # Lower rate than the main list rate based on the number of jobs.
        # We should really be delaying jobs so we don't start them until we're sure we have headroom but I don't expect this site to get innundated.
        rate = max(concurrent_jobs * 3000, 60000)
        last = monotonic_ns() / 1000000
        
        remaining = True
        skip = 0
        
        # Track updated
        deleted = set(ListGame.objects.filter(list=alist).values_list('id', flat=True))
        
        while remaining:
            games, total = backend.fetch_list(alist, skip)
            games = filter(lambda listgame: listgame.game.type != 'accessory', games)
            count = 0
            
            # TODO Fix for "bulk" upserts? (side note: I hate that word "upsert" it just looks ugly)
            for listgame in games:
                if listgame.id in deleted:
                    deleted.remove(listgame.id)
                listgame.game.save()
                listgame.save()
                count += 1
            
            # Their default return amount is 100. If anything is equal to or less than that they return the same results again when trying to page.
            if count == 0 or total <= 100 or skip + count >= total:
                remaining = False
            else:
                # and now we wait.
                now = monotonic_ns() / 1000000
                sleep((rate - (now - last)) / 1000)
                last = now
            
            skip += count
        
        ListGame.objects.filter(id__in=deleted).delete()
    
    def now(self):
        # See above; pickling OK!
        return super().now(self.alist, self.concurrent_jobs)
