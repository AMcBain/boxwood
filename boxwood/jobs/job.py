import traceback
from abc import ABCMeta, abstractmethod
from functools import wraps
from concurrent.futures import ThreadPoolExecutor
from django.db import connection
from django.conf import settings
from boxwood import DJANGO_SHUTDOWN
from boxwood.models.syncing import Syncable

# There do seem to be libraries for background jobs but they seem aimed at recurring tasks, which we might have later.
# None also claim compat with django 3.x so that's kinda not great.
# For the moment we just want to be able to kick off importing of a user's lists sort of ish in the background.
# Yes of course with the GIL and all that it's not really happening in parallel.
pool = ThreadPoolExecutor()

def stop(*args, **kwargs):
    DJANGO_SHUTDOWN.disconnect(stop, sender='system')
    # Do hard exit here? shutdown(wait=False, cancel_futures=True)
    pool.shutdown(wait=True)

DJANGO_SHUTDOWN.connect(stop, sender='system')

# Just a simple parent class to make it easier to write stuff like "class UserListsJob(user=username).now()" type stuff.
class Job(metaclass=ABCMeta):
    
    @abstractmethod
    def do_job():
        pass
    
    def do_job_safely(do_job, on_error, *args, **kwargs):
        try:
            do_job(*args, **kwargs)
        except Exception as err:
            try:
                on_error(err, *args, **kwargs)
            except Exception as err2:
                connection.close()
                raise err2 from err
            # General idea but simpler execution enlightened by https://stackoverflow.com/a/57794016/251262
            connection.close()
            raise err
    
    # FIXME errors don't seem to get shown in the console anywhere we should probably default to logging them here.
    def on_error(error, *args, **kwargs):
        pass
    
    # Not really now now, but whenever the current queue gets finished. So as soon as possible anyway.
    # The args/kwargs are internal for subclasses to call us with their own properties as they expect to get them.
    def now(self, *args, **kwargs):
        # I've probably got this all wrong, but the idea is to treat do_job as just a plain old not-bound function.
        # This way the job itself can't be referenced and nobody can get at things (wihtout work) that belonged to the caller thread/process/whatever.
        return pool.submit(self.do_job_safely.__func__, self.do_job.__func__, self.on_error.__func__, *args, **kwargs)

# Specifically for items that like to indicate when they were last synced and 
class SyncingJob(Job):
    
    def __init__(self, syncable):
        # Check here explicitly or die later?
        if not isinstance(syncable, Syncable):
            raise Exception('SyncingJob requires object to be an instance of Syncable')
        self.syncable = syncable
    
    def do_job(syncable, do_sync, *args, **kwargs):
        do_sync(*args, **kwargs)
        syncable.finish_syncing()
    
    def on_error(error, syncable, *args, **kwargs):
        if settings.DEBUG:
            try:
                print('Error caused for: ' + str(syncable))
                print(traceback.format_exc())
            except:
                print(traceback.format_exc())
        syncable.rollback_syncing()
    
    def now(self, *args, **kwargs):
        self.syncable.start_syncing()
        return super().now(self.syncable, self.do_sync.__func__, *args, **kwargs)
