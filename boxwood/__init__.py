import os
import signal
import sys
from django.dispatch import Signal

DJANGO_SHUTDOWN = Signal()

# On advice of https://stackoverflow.com/a/48540300/251262
def sigint_handler(*args):
    if os.environ.get('RUN_MAIN') == 'true':
        DJANGO_SHUTDOWN.send('system')
    sys.exit(0)

signal.signal(signal.SIGINT, sigint_handler)


# Handle models. Doing it in models/__init__.py results in potentially being called more than once.
# If we put all of it in here, some modules load before the django app registry is complete. :|
models = dict()
__loaded = set()

def model(cls):
    models[cls.__name__] = cls
    return cls

def load_model(name):
    # Let's try not to load everything more than once, yeah?
    if not name in __loaded:
        __loaded.add(name)
        __import__(name)
