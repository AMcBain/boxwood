from django.db import models
from django.db.utils import IntegrityError
from boxwood import model
from boxwood.models.tags import Category, Mechanic

@model
class Game(models.Model):
    id = models.CharField(max_length=24, primary_key=True)
    
    # Some people like breaking stuff: https://boardgamegeek.com/boardgame/130937/game-where-enclosed-short-young-lady-perfect-class
    # because of course they did it twice: https://boardgamegeek.com/boardgame/212901/game-wherein-you-blether-scottish-word-meaning-tal
    # BGG is having none of it and truncates titles at 255 characters. Seems like a good idea to me. Also max_length is a required property.
    name = models.CharField(max_length=255)
    
    # I don't want any of these to be nullable, buuuut that's what they give us sometimers. :(
    published = models.IntegerField(null=True)
    
    min_players = models.IntegerField(null=True)
    
    max_players = models.IntegerField(null=True)
    
    min_playtime = models.IntegerField(null=True)
    
    max_playtime = models.IntegerField(null=True)
    
    min_age = models.IntegerField(null=True)
    
    categories = models.ManyToManyField(Category)
    
    mechanics = models.ManyToManyField(Mechanic)
    
    # Could be someone's name. I give up. If it truncates, it truncates.
    publisher = models.CharField(max_length=255)
    
    # It's a URL so it could be longer. Winging it.
    rules = models.CharField(max_length=255)
    
    bgg_id = models.IntegerField(null=True)
    
    # Presuming the longest type is "expansion" and "accessory".
    type = models.CharField(max_length=10)
    
    # Not required for now. I'm not sure we need these in the database.
    # Even the "preview" field can be at least 3,000 characters long.
    # (See the entry for Century Spice Road.)
    #description = models.TextField()
    
    # Famous last words: due to their use of a CDN these will be shorter than 100 chars.
    image = models.CharField(max_length=100)
    
    @property
    def top_tags(self):
        if self.categories or self.mechanics:
            tags = list(self.categories.all()) if self.categories else []
            if self.mechanics:
                tags.extend(self.mechanics.all())
            tags.sort(key=lambda tag: -tag.rank)
            return tags[0:2]
        return []
    
    @classmethod
    def create(Game, obj, save=True):
        publisher = ''
        if 'primary_publisher' in obj and 'id' in obj['primary_publisher']:
            name = obj['primary_publisher']['name']
            publisher = name if name else ''
        elif len(obj['publishers']) and obj['publishers'][0] != None:
            # Their API doesn't return names here; when they fix that, fix this.
            name = obj['publishers'][0]['id']
            publisher = name if name else ''
        
        game = Game(
            # This is the game ID. For the ID in the of the copy in the list, use listItemId.
            id = obj['id'],
            name = obj['name'],
            published = obj['year_published'] if 'year_published' in obj else None,
            min_players = obj['min_players'],
            max_players = obj['max_players'],
            min_playtime = obj['min_playtime'],
            max_playtime = obj['max_playtime'],
            min_age = obj['min_age'],
            publisher = publisher,
            rules = obj['rules_url'] if 'rules_url' in obj and obj['rules_url'] else '',
            bgg_id = obj['bgg_id'],
            type = obj['type'],
            #description = obj['description_preview'].strip(),
            image = obj['images']['medium'],
        )
        
        # Default to saving because these are used by other things, and the following may fail if it's false.
        if save:
            game.save()
        
        if obj['mechanics']:
            mechanics = list(map(lambda mechanic: Mechanic.create(mechanic), obj['mechanics']))
            try:
                game.mechanics.set(mechanics)
            except (IntegrityError, ValueError):
                # This happens when there's a mechanic being asked to save that we don't already have or because the game doesn't exist in the DB.
                # django is supposed to add it, but the add logic still assumes it exists and just tries to create a mapping to the non-existent mechanic.
                # Instead, we'll have to do it the super slow way.
                existing = Game.objects.get(pk=obj['id'])
                if existing:
                    existing = set(map(lambda mechanic: mechanic.id, existing.mechanics.all()))
                    for mechanic in mechanics:
                        if mechanic in existing:
                            existing.remove(mechanic.id)
                        else:
                            try:
                                game.mechanics.add(mechanic)
                            except ValueError:
                                try:
                                    # Don't overwrite the name field of existing items.
                                    # The add() above doesn't do save() on items, just uses the ID, so it's safe.
                                    mechanic.refresh_from_db(fields=['name'])
                                except Mechanic.DoesNotExist:
                                    # Last resort. Stick the ghost mechanic into the DB with a name == ID.
                                    mechanic.save()
                                game.mechanics.add(mechanic)
                    # Remove leftovers
                    if existing:
                        game.mechanics.remove(*existing)
        
        categories = map(lambda category: Category.create(category), obj['categories'])
        if obj['categories']:
            try:
                game.categories.set(categories)
            except (IntegrityError, ValueError):
                # Should really be deduplicated with the one above so we don't fetch it twice. That said, both failing is unlikely.
                existing = Game.objects.get(pk=obj['id'])
                if existing:
                    existing = set(map(lambda category: categories.id, existing.categories.all()))
                    for category in categories:
                        if category in existing:
                            existing.remove(category.id)
                        else:
                            try:
                                game.categories.add(category)
                            except ValueError:
                                try:
                                    category.refresh_from_db(fields=['name'])
                                except Category.DoesNotExist:
                                    category.save()
                                game.categories.add(category)
                    # Remove leftovers
                    if existing:
                        game.categories.remove(*existing)
        
        return game
