import re
import json
from django.db import models
from django.contrib.auth.models import User
from boxwood import model
from boxwood.models.games import Game
from boxwood.models.syncing import Syncable

@model
class AtlasList(Syncable):
    id = models.CharField(max_length=24, primary_key=True)
    
    # We could extend the user object, but we chose not to do that with profiles so...
    user = models.ForeignKey(
        User,
        on_delete = models.CASCADE,
        # Admin user, hopefully. Migrations won't pass without allowing this to be null (ugh) or have a default.
        # This TOTALLY won't come back to bite us later. Solution: regenerate migrations from scratch.
        default = 1
    )
    
    # Presuming... but also don't care, because if you make a list with a really long name we're not going to display all of it anyway.
    name = models.CharField(max_length=255)
    
    # So we can link back to it somehow if we want.
    url = models.CharField(max_length=255)
    
    # This is only on our end, indicates whether a user has marked it as lendable or not.
    # If they delete the list on the other side and we do so here when we notice, then this is lost with it.
    lendable = models.BooleanField(default=False)
    
    count = models.IntegerField()
    
    # They could be longer than this, but most likely they'll be on a CDN and < 100.
    image = models.CharField(max_length=100)
    
    def __str__(self):
        return self.name + ' by ' + self.user.username
    
    @classmethod
    def create(AtlasList, user, obj):
        return AtlasList(
            id = obj['id'],
            user = user,
            name = obj['name'],
            url = obj['url'],
            lendable = False,
            count = obj['gameCount'],
            # Empty lists don't have a preview image.
            image = obj['images']['medium'] if 'medium' in obj['images'] else '/static/images/robert-katzki-8WsB76QE7ic-unsplash.jpg'
        )

@model
class ListGame(models.Model):
    id = models.CharField(max_length=24, primary_key=True)
    
    list = models.ForeignKey(
        'AtlasList',
        related_name = 'games',
        on_delete = models.CASCADE,
        # Due to the requirement of allowing suggestions.
        blank = True,
        null = True,
    )
    
    # on_delete should be...? Deleting of games is bad, but deleting of ListGame is OK.
    game = models.ForeignKey(
        'Game',
        related_name = '+',
        on_delete = models.CASCADE,
    )
    
    # The idea would be for people to use this to mention special things about this copy.
    note = models.TextField()
    
    # See LibraryGame#barcode
    barcode = models.CharField(
        max_length=13,
        null = True,
        blank = True,
    )
    
    def __str__(self):
        return self.game.name + (' (' + str(self.game.published) + ')' if self.game.published else '')
    
    @classmethod
    def create(ListGame, alist, obj):
        return ListGame(
            id = obj['listItemId'],
            list = alist,
            # This will fail on save() so look up existing game first rather than creating a new object?
            game = Game.create(obj),
            note = obj['note'] if 'note' in obj else ''
        )
    
    @classmethod
    def placeholder(ListGame, alist, listItemId, obj):
        return ListGame(
            id = listItemId,
            list = alist,
            game = obj,
            note = 'System-generated Placeholder'
        )
