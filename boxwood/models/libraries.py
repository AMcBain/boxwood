from datetime import datetime, timezone
from django.db import models
from django.contrib import admin
from django.forms import Textarea, ModelForm
from django.utils.crypto import get_random_string
from boxwood import model
from boxwood.models.lists import ListGame
from boxwood.models.games import Game

ID_CHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(-_=+)'

# Represents our event game libraries.
@model
class Library(models.Model):
    
    # Most libraries are probably doing to be called "RadCon" or "Tabletop Day" etc.
    # 65 characters is being generous.
    name = models.CharField(max_length=65)
    
    # We can make it bigger if people are intent on being Shakespeare.
    description = models.CharField(
        max_length = 255,
        help_text = 'Max 255 characters.'
    )
    
    # Photo op! Smile for the camera.
    image = models.FileField(upload_to='') # just /media/
    
    games = models.ManyToManyField(ListGame, through='LibraryGame')
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = 'Libraries'

# Contains all the meta info above and beyond a standard game that only applies to library "copies".
@model
class LibraryGame(models.Model):
    
    library = models.ForeignKey(
        'Library',
        on_delete=models.CASCADE,
    )
    
    listgame = models.ForeignKey(
        'ListGame',
        on_delete=models.CASCADE,
    )
    
    # Does this belong here? It seems like something that should be on ListGame.
    # It depends on how we want to get these barcodes into the system.
    # If we want people to fix their own items, it should be on ListGame.
    # Could list it twice, and use this one if it's populated otherwise ListGame's field. Yucky though.
    # 
    # EAN-13; but products that don't have one might need a custom barcode.
    barcode = models.CharField(
        max_length=13,
        null = True,
        blank = True,
    )
    
    checked_out = models.BooleanField()
    
    suggested = models.BooleanField()
    
    def is_virtual(self):
        return not self.suggested and not self.listgame.list
    
    def __str__(self):
        name = self.listgame.game.name
        if self.listgame.list:
            name += " ("
            if self.listgame.list.user:
                name += "Owned by "
                name += self.listgame.list.user.username
            name += " from "
            name += self.listgame.list.name
            name += ")"
        return name
    
    @classmethod
    def create(LibraryGame, library, obj, suggested):
        if isinstance(obj, Game):
            # Create an ownerless game. It'll get cleaned up later when the request is accepted or rejected.
            obj = ListGame.placeholder(
              None,
              # Make sure we don't clash with anything in the Atlas.
              # They don't seem to use anything other than A-Z, a-z, and 0-9.
              # We go overboard just in case they add more later.
              '~' + get_random_string(23, ID_CHARS),
              obj,
            )
            obj.save()
        
        return LibraryGame(
            library = library,
            listgame = obj,
            checked_out = False,
            suggested = suggested,
        )
    
    class Meta:
        verbose_name = 'Library Game'
        verbose_name_plural = 'Library Games'
        unique_together = ['library', 'listgame']

@model
class LibraryLogEntry(models.Model):
    
    # TODO Unless we care to keep track of exactly whose copy got played, this can be switched directly to just Game (not ListGame).
    game = models.ForeignKey(
        'LibraryGame',
        on_delete=models.CASCADE,
    )
    
    # Can't be null. This is the entire point of the log entry.
    checked_out = models.DateTimeField()
    
    # How long are badge numbers anyway? Do some places use non-numeric badge numbers?
    badge = models.CharField(max_length=10)
    
    checked_in = models.DateTimeField(null=True)
    
    def check_in(self):
        self.checked_in = datetime.now(tz=timezone.utc)
        self.save()
    
    @classmethod
    def create(LibraryLogEntry, game, badge):
        return LibraryLogEntry(
            game = game,
            checked_out = datetime.now(tz=timezone.utc),
            badge = badge,
        )
    
    class Meta:
        verbose_name_plural = 'Library Log Entries'


# Backend admin area
@admin.register(Library)
class LibraryAdmin(admin.ModelAdmin):
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'description':
            kwargs['widget'] = Textarea
        return super().formfield_for_dbfield(db_field,**kwargs)

@admin.register(LibraryGame)
class LibraryGameAdmin(admin.ModelAdmin):
    pass

@admin.register(LibraryLogEntry)
class LibraryLogEntryAdmin(admin.ModelAdmin):
    
    list_display = ['game', 'checked_out', 'badge', 'checked_in']
    
    readonly_fields = ['game', 'checked_out', 'badge', 'checked_in']


# Forms
class LibraryForm(ModelForm):
    class Meta:
        model = Library
        exclude = ['games']
        widgets = {
            'description': Textarea(),
        }
