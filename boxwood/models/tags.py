from django.db import models
from django.contrib import admin
from boxwood import model

@model
class Tag(models.Model):
    
    id = models.CharField(max_length=10, primary_key=True)
    
    # Used to sort tags so we know which to show when there's limited space.
    rank = models.IntegerField(
        default = 0,
        help_text = 'Sort order to determine which items are more important to show if there\' too many.'
    )
    
    # Longest is currently 25 characters.
    name = models.CharField(max_length=30)
    
    def __str__(self):
        return self.name
    
    class Meta:
        abstract = True

# Yeah we could just add a type field to the above and go that route.
@model
class Category(Tag):
    @classmethod
    def create(Category, obj):
        return Category(
            id = obj['id'],
            # django only seems to pay attention to the ID when setting up new ListGame objects.
            # So we don't have to worry about overwrites but do want to try to ensure they save properly if they're new and we don't have them yet.
            name = obj['name'] if 'name' in obj else obj['id'],
        )
    
    @classmethod
    def lookup(Category, tags):
        return []
    
    class Meta:
        verbose_name_plural = 'Categories'

@model
class Mechanic(Tag):
    @classmethod
    def create(Mechanic, obj):
        return Mechanic(
            id = obj['id'],
            # See above.
            name = obj['name'] if 'name' in obj else obj['id'],
        )
    
    @classmethod
    def lookup(Mechanic, tags):
        return []


# Backend admin area
class TagAdmin(admin.ModelAdmin):
    
    # Because we get this from their API so any changes would overwrite it.
    readonly_fields = ('name', 'id')
    
    def get_fields (self, request, obj=None, **kwargs):
        fields = super().get_fields(request, obj, **kwargs)
        fields.remove('id')
        fields.insert(0, 'id')
        fields.remove('name')
        fields.insert(1, 'name')
        return fields
    
    # These all come from their API
    def has_add_permission(self, request):
        return False

@admin.register(Category)
class CategoryAdmin(TagAdmin):
    pass

@admin.register(Mechanic)
class MechanicAdmin(TagAdmin):
    pass
