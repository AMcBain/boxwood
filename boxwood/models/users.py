from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.forms import ModelForm, Textarea
from django.conf import settings
from boxwood import model
from boxwood.models.roles import Role, RoleCheckboxSelectMultiple
from boxwood.models.syncing import Syncable, SyncableAdmin
from boxwood.jobs.lists import UpdateUserListsJob

# This is as opposed to extending the main user object and replacing it with our own.
# We can and it's not hard to do but this seems reasonable too.
@model
class Profile(Syncable):
    
    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE,
        primary_key = True,
    )
    
    # Cover stuff we don't? Just text. No fancy bb-code, wiki, or markdown handling.
    about = models.CharField(
        max_length = 500,
        default = '',
        blank = True,
        help_text = '(Optional) Max 500 characters. Anything extra you want to include on your user page.'
    )
    
    # Some people might be around the Tri-Cities but not in it (Walla Walla, NE Oregon)
    # "Taumatawhakatangihangakoauauotamateaturipukakapikimaungahoronukupokaiwhenuakitnatahu, New Zealand"
    location = models.CharField(
        max_length = 100,
        default = '',
        blank = True,
        help_text = '(Optional) Not your address. Just a general label for where you live, if you\'d like.'
    )
    
    # There are apparently some "legacy" ones that could be longer. If they complain, I'll fix it.
    # https://boardgamegeek.com/thread/1258396/article/17278073#17278073
    bgg_username = models.CharField(
        verbose_name = 'BGG Username',
        max_length = 20,
        default = '',
        blank = True,
        help_text = '(Optional) Your Board Game Geek username.'
    )
    
    # I think it's 20... but can't find proof. Longest real-looking names and some usernames seem to all top out at 20.
    bga_username = models.CharField(
        verbose_name = 'BGA Username',
        max_length = 20,
        default = '',
        blank = True,
        help_text = '(Optional) Your Board Game Arena username.'
    )
    
    # I dunno, it's an account that's usually considered more public than Facebook so, why not?
    twitter_handle = models.CharField(
        verbose_name = 'Twitter',
        max_length = 15,
        default = '',
        blank = True,
        help_text = '(Optional) Your Twitter handle.'
    )
    
    roles = models.ManyToManyField(
        Role,
        blank = True,
        help_text = '(Optional) Tell us about interests, skills, hobbies, relations, etc. Sorry for the long list.'
    )
    
    # From the Atlas API
    image = models.CharField(
        max_length = 100,
        default = '',
        blank = True,
    )
    
    # Also from the Atlas. Seriously. WTF.
    image_css = models.CharField(
        max_length = 100,
        default = '',
        blank = True,
    )
    
    board = models.BooleanField(
        default = False,
        help_text = 'For admins only, to indicate who is an official board member, if wanted.'
    )
    
    advisor = models.BooleanField(
        default = False,
        help_text = 'For admins only, to indicate who is an official advisor, if wanted.'
    )
    
    verified = models.BooleanField(
        default = False,
        help_text = 'For staff and admins only, to indicate who can contribute suggestions or have games included in libraries.'
    )
    
    def is_ok_to_sync(self):
        return self.verified and super().is_ok_to_sync()
    
    def is_verification_ready(self):
        try:
            return self.verified or self.verification_request and len(self.verification_request.staff_context) > 0
        except Profile.verification_request.RelatedObjectDoesNotExist:
            # Handle users that were rejected (verified = False) but their request was deleted after the rejection.
            return True
    
    @property
    def about_paragraphs(self):
        return self.about.split('\n')
    
    def __str__(self):
        return self.user.username + '\'s Profile'

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    # Second check is to take care of the initially created superuser.
    if created or not hasattr(instance, 'profile'):
        Profile.objects.create(user=instance)
        if settings.VERIFY_NEW_USERS:
            UserVerificationRequest.create(user=instance)
            user.verification_request.save()
        else:
            user.profile.verified = True
            UpdateUserListsJob(instance).now()
    instance.profile.save()


# This could have a field for the acceptance (or rejection) but no real reason to keep logs of this.
# So unless there's any reason found, just delete it on resolution and update user.profile.verified
@model
class UserVerificationRequest(models.Model):
    
    profile = models.OneToOneField(
        Profile,
        on_delete = models.CASCADE,
        primary_key = True,
        related_name = 'verification_request',
    )
    
    staff_context = models.CharField(
        max_length = 500,
        default = '',
        blank = True,
        help_text = (
            'Required. Max 500 characters. ' +
            'This is only to let the staff know who you are and your relation to or interest in ' + settings.ORG_NAME + ' for approval review. ' +
            'You\'ll be able to set your public profile later.'
         )
    )


# Backend admin area
@admin.register(Profile)
class ProfileAdmin(SyncableAdmin):
    
    filter_horizontal = ('roles',)
    
    exclude = ('is_syncing', 'last_synced', 'image', 'image_css')
    
    def get_fields (self, request, obj=None, **kwargs):
        fields = super().get_fields(request, obj, **kwargs)
        fields.remove('user')
        fields.insert(0, 'user')
        return fields
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'about':
            kwargs['widget'] = Textarea
        elif db_field.name == 'roles':
            kwargs['widget'] = RoleCheckboxSelectMultiple()
        return super().formfield_for_dbfield(db_field,**kwargs)


# Forms
class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name']
        labels = {
            'first_name': 'Name',
        }
        help_texts = {
            'first_name': '(Optional) It\'d be very helpful if you fill it in if your username isn\'t close to your actual name.'
        }

class UserProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude = ['user', 'is_syncing', 'last_synced', 'image', 'image_css', 'board', 'advisor', 'verified']
        widgets = {
            'roles': RoleCheckboxSelectMultiple(),
            'about': Textarea(),
        }

class UserVerificationRequestForm(ModelForm):
    class Meta:
        model = UserVerificationRequest
        exclude = ['user']
        labels = {
            'staff_context': 'Tell us a little bit about why you signed up',
        }
        widgets = {
            'staff_context': Textarea(),
        }
