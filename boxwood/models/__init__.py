from pkgutil import walk_packages
from boxwood import load_model, models

# We can't do this completely in the parent __init__.py as some submodules reference other apps and the app registry isn't loaded yet.
# We can't put it all in here, because then every time we're called the mappings would reset. Dynamic languages are fun, aren't they?


# This is the magic. Finds all packages, modules, sub-stuff, etc. then imports it to run the decorators.
# Care must be taken not to have stuff in this package that shouldn't be auto-imported.
for finder, name, ispkg in walk_packages(path=__path__, prefix='boxwood.models.', onerror=lambda x: None):
    load_model(name)
    # This is how we try to avoid the "partially initialized module" error.
    __all__ = list(models.keys())
    globals().update(models)

__all__ = list(models.keys())
globals().update(models)
