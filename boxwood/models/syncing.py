from datetime import datetime, timezone, timedelta
from django.db import models
from django.contrib import admin
from boxwood import model

# Does anyone remember that German? TV commercial that was on YouTube with the guy learning English while working as a coast guard equivalent?
# "What are you [si]nking about?" https://www.youtube.com/watch?v=zkalf0odHs8&t=5
@model
class Syncable(models.Model):
    
    # Whether a sync is active right now so we don't have to fiddle with dates until we're done.
    is_syncing = models.BooleanField(default=False)
    
    # Last time we updated this list so we can make sure we don't do it too often.
    last_synced = models.DateTimeField(null=True)
    
    # Ideally this is so if we do syncs based on things that aren't just recurring background jobs we don't run it too soon.
    # Or, if we increase the interval from 24 hours but run the job more often, this will help cut down on what we ask to sync.
    # The Atlas says games once per 24 hours, at best, though I think this is possibly in reference to their BGG background sync.
    def is_ok_to_sync(self):
        return not self.is_syncing and (not self.last_synced or self.last_synced + timedelta(hours=24) <= datetime.now(tz=timezone.utc))
    
    # Silly? Yes, but if we add more complicated stuff later then this is easy to update and it's not like it costs us really to do this.
    def start_syncing(self):
        self.is_syncing = True
        self.save()
    
    def rollback_syncing(self):
        self.is_syncing = False
        self.save()
    
    def finish_syncing(self):
        self.last_synced = datetime.now(tz=timezone.utc)
        self.is_syncing = False
        self.save()

    class Meta:
        abstract = True

# Used by all Syncables for the admin to hide our internal fields.
class SyncableAdmin(admin.ModelAdmin):
    
    # We don't want to show these; they're for internal use only.
    exclude = ('is_syncing', 'last_synced')
    
    def get_fields (self, request, obj=None, **kwargs):
        return super().get_fields(request, obj, **kwargs)
