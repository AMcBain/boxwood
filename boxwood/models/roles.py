from django.db import models
from django.contrib import admin
from django.forms import CheckboxSelectMultiple
from boxwood import model

# Using the word "role" in the generic sort of way Discord does, as a user attribute tag.

@model
class Role(models.Model):
    
    id = models.CharField(max_length=10, primary_key=True)
    
    # This was based on tags for length, but it could be shorter.
    name = models.CharField(max_length=30)
    
    # Not intended for long winded explanations.
    description = models.CharField(max_length=128)
    
    # Some have a reasonable place to link to.
    link = models.CharField(max_length=255)
    
    # Not so crazy. Intended for a hex code.
    foreground = models.CharField(
        max_length = 7,
        help_text = 'Values starting with a \'.\' will be treated as CSS class names. (Useful for using existing Bootstrap color helpers.) Otherwise the value is assumed to be a valid CSS <color> value.'
    )
    
    # Technically doesn't need to be more than 7 for hex codes but... linear-gradients!
    background = models.CharField(
        max_length = 255,
        help_text = 'Values starting with a \'.\' will be treated as a CSS class name. (Useful for using existing Bootstrap color helpers.) Otherwise the value is assumed to be a valid CSS background value.'
    )
    
    @property
    def classes(self):
        classes = ''
        if self.foreground and self.foreground.startswith('.'):
            classes += self.foreground[1:]
        if self.background and self.background.startswith('.'):
            classes += ' ' + self.background[1:]
        return classes

    @property
    def styles(self):
        styles = ''
        # Yeah this can be used to insert more things than should be if they're short enough, but meh.
        if self.foreground and not self.foreground.startswith('.'):
            styles += 'color: ' + self.foreground
        if self.background and not self.background.startswith('.'):
            styles += '; background: ' + self.background
        return styles
    
    def __str__(self):
        return self.name


# Forms
# We will definitely want something better if we intend to have more of these.
# It's quite a long list as it is but this was better than <select multiple>...</select>
class RoleCheckboxSelectMultiple(CheckboxSelectMultiple):
    
    option_template_name = 'widgets/checkbox_option.html'
    
    def create_option(self, name, value, *args, **kwargs):
        option = super().create_option(name, value, *args, **kwargs)
        option['label'] = value.instance.name
        option['object'] = value.instance
        return option
