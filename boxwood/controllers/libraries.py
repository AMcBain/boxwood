from datetime import date, timedelta
from django.db import connection
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.views.decorators.http import require_POST
from django.http import HttpResponseBadRequest
from boxwood.urls import route
from boxwood.models.libraries import Library, LibraryGame, LibraryLogEntry, LibraryForm
from boxwood.models.games import Game
from boxwood.models.lists import ListGame
from boxwood.forms.filters import DateRangeForm
from boxwood.forms.libraries import LibraryAddGameForm
from boxwood.views import render_html, paged, filtered, verified
from boxwood.api import backend
from boxwood.forms.filters import FiltersForm

# First so longer URLs don't match 'new' as a library id.
@route(r'^library/new/post$')
@require_POST
def new_post(request):
    if request.user.is_superuser or request.user.is_staff:
        
        form = LibraryForm(request.POST, request.FILES)
        if form.is_valid():
            library = form.save()
            return redirect('boxwood.controllers.libraries.library', library.id)
        
        # save errors and redirect back to the form.
        request.session['errors'] = []
        request.session['data'] = request.POST
        request.session['files'] = request.FILES
        
        return redirect('boxwood.controllers.libraries.new')
    raise PermissionDenied('Sorry, only staff can add new libraries.')

@route(r'^library/new$')
@render_html
@verified
def new(request):
    if request.user.is_superuser or request.user.is_staff:
        errors = request.session['errors'] if 'errors' in request.session else []
        
        if 'errors' in request.session:
            form = LibraryForm(request.session['data'], request.session['files'])
            form.is_valid() # force validation
            del request.session['data']
            del request.session['files']
            del request.session['errors']
        else:
            form = LibraryForm()
        
        return {
            'form': form,
            'errors': errors,
        }
    raise PermissionDenied('Sorry, only staff can add new libraries.')

@route(r'^library/(?P<library_id>\w+)/stats$')
@render_html
@verified
def stats(request, library_id):
    # For now? At least until it is cached or known to be fast enough for all users.
    if request.user.is_staff or request.user.is_superuser:
        
        data = list()
        datum = None
        
        if 'start' in request.GET or 'end' in request.GET:
            form = DateRangeForm(request.GET)
            if form.is_valid():
                ustart = form.cleaned_data.get('start')
                uend = form.cleaned_data.get('end')
                
                if uend < ustart:
                    start = uend
                    end = ustart
                else:
                    start = ustart
                    end = uend
            else:
                start = form.get_initial_for_field(form.fields['start'], 'start')
                end = form.get_initial_for_field(form.fields['end'], 'end')
        else:
            form = DateRangeForm()
            start = form.get_initial_for_field(form.fields['start'], 'start')
            end = form.get_initial_for_field(form.fields['end'], 'end')
        
        # Performance enhancement cut-off.
        max_start = end - timedelta(days=14)
        if max_start > start:
            start = max_start
        
        # FIXME Not cross-compatible with other databases than SQLite3
        # FIXME Remove all items with a checkout under 5 minutes.
        with connection.cursor() as cursor:
            cursor.execute('''
                select count(id) as count, strftime("%%Y-%%m-%%d", checked_out) as start_of_day, strftime("%%H", checked_out) as hour, strftime("%%Y-%%m-%%dT%%H:00:00Z", checked_out) as start_of_hour
                from boxwood_librarylogentry
                where date(checked_out) >= date(%s) and date(checked_out) <= date(%s)
                group by start_of_hour
                order by start_of_hour
            ''', [start.isoformat(), end.isoformat()])
            
            for row in cursor.fetchall():
                if not datum or datum['date'] != row[1]:
                    
                    delta = timedelta(days=1)
                    then = date.fromisoformat(datum['date']) + delta if datum else start
                    
                    while then.isoformat() != row[1]:
                        data.append({
                            'date': then.isoformat(),
                            'hours': [],
                        })
                        then += delta
                    
                    datum = {
                        'date': row[1],
                        'hours': [],
                    }
                    data.append(datum)
                
                datum['hours'].append({
                    'hour': row[2],
                    'count': row[0],
                })
        
        games = Game.objects.raw('''
            select boxwood_game.*, count(lle.id) as checkouts
            from boxwood_librarylogentry lle
            join boxwood_librarygame yg on yg.id = lle.game_id
            join boxwood_listgame lg on lg.id = yg.listgame_id
            join boxwood_game on boxwood_game.id = lg.game_id
            where date(lle.checked_out) >= date(%s) and date(lle.checked_out) <= date(%s)
            group by boxwood_game.id
            order by checkouts desc, boxwood_game.name, boxwood_game.publisher
            limit 10
        ''', [start.isoformat(), end.isoformat()])
        
        # TODO List games with most checkouts under 5 minutes (might indicate difficulties) / lack of interest.
        return {
            'form': form,
            'library': Library.objects.get(pk=library_id),
            'checkouts': {
                'checkouts': data,
            },
            'top_10_games_played': games,
        }
    raise PermissionDenied('Sorry, library stats are staff only feature for now.')

@route(r'^library/(?P<library_id>\w+)/game/checkout$')
@require_POST
def check_out(request, library_id):
    if request.user.is_staff or request.user.is_superuser:
        if not 'listgame_id' in request.POST or not 'badge' in request.POST:
            return HttpResponseBadRequest(reason='Request must contain a listgame_id and badge parameter.')
        
        librarygame = LibraryGame.objects.get(
            library__id = library_id,
            listgame__id = request.POST['listgame_id'],
        )
        
        # Try to find an unclosed log entry. If one exists then fail.
        try:
            LibraryLogEntry.objects.get(
                game = librarygame,
                checked_in = None,
            )
            return HttpResponseBadRequest(reason='Game is already checked out!')
        except LibraryLogEntry.DoesNotExist:
            pass
        
        entry = LibraryLogEntry.create(librarygame, request.POST['badge'])
        entry.save()
        
        # Maybe work this out based on LibraryLogEntry instead?
        # However really the log is just an artifact; that is it doesn't matter if it succeeds as long as we know whether the game is out or not.
        librarygame.checked_out = True
        librarygame.save()
        
        return redirect(request.POST['next'], library_id)
    raise PermissionDenied('Sorry, only staff can check out games from libraries.')

@route(r'^library/(?P<library_id>\w+)/game/checkin$')
@require_POST
def check_in(request, library_id):
    if request.user.is_staff or request.user.is_superuser:
        if not 'listgame_id' in request.POST:
            return HttpResponseBadRequest(reason='Request must contain a listgame_id parameter.')
        
        librarygame = LibraryGame.objects.get(
            library__id = library_id,
            listgame__id = request.POST['listgame_id'],
        )
        
        try:
            # If we have multiple copies of a game, the library attendant might not grab the right copy to check back in from the list.
            # This tries to look it up by badge number instead and ensure we get the right one.
            entry = LibraryLogEntry.objects.get(
                game__listgame__game__id = librarygame.listgame.game.id,
                badge = request.POST['badge'],
                checked_in = None,
            )
        except LibraryLogEntry.DoesNotExist:
            # Will fail with an index error if there is no existing match.
            # That's intended. This shouldn't be called if there's no checkouts.
            entry = LibraryLogEntry.objects.filter(
                game = librarygame,
                checked_in = None,
            )[0]
        entry.check_in()
        
        librarygame.checked_out = False
        librarygame.save()
        
        return redirect(request.POST['next'], library_id)
    raise PermissionDenied('Sorry, only staff can check in games from libraries.')

@route(r'^library/(?P<library_id>\w+)/game/barcode$')
@require_POST
def set_barcode(request, library_id):
    if request.user.is_staff or request.user.is_superuser:
        if 'librarygame_id' in request.POST and 'barcode' in request.POST:
            librarygame = LibraryGame.objects.get(pk=request.POST['librarygame_id'])
            librarygame.barcode = request.POST['barcode'].strip()
            librarygame.save()
            request.session['successes'] = ['Success! Barcode added.']
            return redirect(request.POST['next'], library_id)
        return HttpResponseBadRequest(reason='Request must contain a librarygame_id and a barcode parameter.')
    raise PermissionDenied('Sorry, only staff can set barcodes on library games.')

@route(r'^library/(?P<library_id>\w+)/game/remove$')
@require_POST
def remove_from_library(request, library_id):
    librarygame = LibraryGame.objects.get(
        library__id = library_id,
        listgame__id = request.POST['listgame_id'],
    )
    if request.user.is_staff or request.user.is_superuser or (librarygame.listgame.list and librarygame.listgame.list.user.username == request.user.username):
        # Don't leave orphans in case of ownerless games. (requests)
        if not librarygame.listgame.list:
            librarygame.listgame.delete()
        librarygame.delete()
        
        if request.user.is_staff or request.user.is_superuser:
            request.session['successes'] = ['Success! Game removed.']
        else:
            request.session['successes'] = ['Success! We removed your game from the library.']
        
        return redirect(request.POST['next'], library_id)
    raise PermissionDenied('Sorry, only staff or owners of a game in the library can remove games from libraries.')

@route(r'^library/(?P<library_id>\w+)/add/confirm_post$')
@require_POST
def confirm_game_post(request, library_id):
    if request.user.is_authenticated:
        
        suggested = not (request.user.is_staff or request.user.is_superuser)
        library = Library.objects.get(pk=library_id)
        already_have = False
        
        if 'listgame_id' in request.POST:
            listgame = ListGame.objects.get(pk=request.POST['listgame_id'])
            # Check if we already have it.
            try:
                librarygame = LibraryGame.objects.get(
                    library = library,
                    listgame = listgame,
                )
                # These come from handling suggestions of someone from their own library.
                if librarygame.suggested:
                    librarygame.suggested = False
                else:
                    already_have = True
            except LibraryGame.DoesNotExist:
                # Check to see if we're fulfilling a placeholder.
                if 'librarygame_id' in request.POST:
                    librarygame = LibraryGame.objects.get(pk=request.POST['librarygame_id'])
                    librarygame.listgame = listgame
                else:
                    try:
                        librarygame = LibraryGame.objects.filter(
                            library = library,
                            listgame__game__id = listgame.game.id,
                            suggested = True,
                        )[0]
                        # Resolve a random user request.
                        librarygame.listgame = listgame
                        librarygame.suggested = False
                    except IndexError:
                        # We don't have it, even as a request. Let's create it.
                        librarygame = LibraryGame.create(library, listgame, suggested)
        elif 'game_id' in request.POST:
            game_id = request.POST['game_id']
            try:
                librarygame = LibraryGame.objects.filter(
                    library = library,
                    suggested = True,
                    listgame__game__id = game_id,
                )[0]
                librarygame.suggested = False
            except IndexError:
                game = Game.objects.get(pk=game_id)
                librarygame = LibraryGame.create(library, game, suggested)
        else:
            return HttpResponseBadRequest(reason='Request must contain a listgame_id or game_id parameter.')
        
        if already_have:
            # FIXME turn into formatter strings.
            if suggested:
                request.session['successes'] = ['Hey you\'re in luck! The ' + library.name + ' library already has ' + listgame.game.name + '.']
            else:
                request.session['successes'] = ['Success! Nothing to do here. The ' + library.name + ' library already has ' + listgame.game.name + '.']
        else:
            try:
                librarygame.save()
                
                if suggested:
                    request.session['successes'] = ['Thanks! Your we\'ve received your suggestion and will review it soon.']
                else:
                    request.session['successes'] = ['Success! Game added.']
            except:
                # Just in case, so we don't have any orphans.
                if not librarygame.listgame.list:
                    librarygame.listgame.delete()
                
                if suggested:
                    request.session['errors'] = ['Womp womp. There was an error adding the game.']
                else:
                    request.session['errors'] = ['Womp womp. There was an error suggesting the game.']
        
        return redirect(request.POST['next'], library_id)
    return PermissionDenied('Sorry, you need to be logged in to suggest new games.')

@route(r'^library/(?P<library_id>\w+)/add/confirm$')
@render_html
@require_POST
def confirm_game(request, library_id):
    if request.user.is_authenticated:
        if request.user.is_staff or request.user.is_superuser:
            
            library = Library.objects.get(pk=library_id)
            librarygame = None
            existing = []
            
            if 'librarygame_id' in request.POST:
                librarygame = LibraryGame.objects.get(pk=request.POST['librarygame_id'])
                game_id = librarygame.listgame.game.id
            else:
                game_id = request.POST['game_id']
                existing = LibraryGame.objects.filter(
                    library = library,
                    suggested = False,
                    listgame__game__id = game_id,
                ).values_list('listgame__id', flat=True)
            
            games = ListGame.objects.filter(
                list__lendable = True,
                game__id = game_id,
            ).exclude(id__in=existing)
            
            # FIXME This should be paged, but because it's a post... meh. Don't want to work that out yet.
            #       Hopefully we won't have a billion copies between everyone of a given game.
            return {
                'from_librarygame': librarygame.id if librarygame else None,
                'game_id': game_id,
                'already_have': bool(existing),
                'library': library,
                'games': games,
                'next': request.POST['next'] if 'next' in request.POST else None,
            }
        return confirm_game_post(request, library_id)
    return PermissionDenied('Sorry, you need to be logged in to suggest new games.')

# We can just do a GET request to the next URL (GET to itself), but this is maybe a little cleaner looking.
@route(r'^library/(?P<library_id>\w+)/add/post$')
@render_html
@verified
def add_game_post(request, library_id):
    if request.user.is_authenticated:
        form = LibraryAddGameForm(request.POST)
        if form.is_valid():
            games, total = backend.find_games(request.POST['criteria'])
            request.session['games'] = list(games)
            request.session['games_more'] = total > 10
        
        # save data and redirect back to the form.
        request.session['errors'] = []
        request.session['data'] = request.POST
        
        return redirect('boxwood.controllers.libraries.add_game', library_id)
    return PermissionDenied('Sorry, you need to be logged in to suggest new games.')

@route(r'^library/(?P<library_id>\w+)/add$')
@render_html
@verified
def add_game(request, library_id):
    if request.user.is_authenticated:
        library = Library.objects.get(pk=library_id)
        dirty = 'errors' in request.session
        errors = request.session['errors'] if 'errors' in request.session else []
        games = request.session['games'] if 'games' in request.session else []
        toomany = request.session['games_more'] if 'games_more' in request.session else False
        
        if dirty:
            form = LibraryAddGameForm(request.session['data'])
            form.is_valid() # force validation
            del request.session['data']
            del request.session['errors']
        else:
            form = LibraryAddGameForm()
    
        if 'games' in request.session:
            del request.session['games']
            del request.session['games_more']
    
        return {
            'library': library,
            'form': form,
            'dirty': dirty,
            'games': games,
            'toomany': toomany,
        }
    return PermissionDenied('Sorry, you need to be logged in to suggest new games.')

@route(r'^libraries/add$')
@render_html
@verified
def add_to_library(request):
    if request.user.is_authenticated:
        libraries = Library.objects.all()
        
        if not 'listgame_id' in request.POST and not 'game_id' in request.POST:
            return HttpResponseBadRequest(reason='Request must contain a listgame_id or game_id parameter.')
        
        # FIXME This should be paged; paged + post = bleh so... at some point.
        return {
            'listgame_id': request.POST['listgame_id'] if 'listgame_id' in request.POST else None,
            'game_id': request.POST['game_id'] if 'game_id' in request.POST else None,
            'next': request.POST['next'],
            'libraries': libraries,
        }
    return PermissionDenied('Sorry, you need to be logged in to suggest new games.')

@route(r'^library/(?P<library_id>\w+)/checklist/?$')
@render_html
@verified
def checklist(request, library_id):
    if request.user.is_authenticated:
        library = Library.objects.get(pk=library_id)
        
        games = LibraryGame.objects.filter(
            library = library,
            listgame__list__user = request.user,
            suggested = False,
        ).order_by('listgame__game__name', 'listgame__game__published', 'id')
        
        return {
            'library': library,
            'games': games,
        }
    return PermissionDenied('Sorry, you need to be logged in to view your packlist.')

@route(r'^library/(?P<library_id>\w+)/?$')
@render_html
@paged('games')
@filtered('games', 'listgame__game__')
@verified
def library(request, library_id):
    errors = request.session['errors'] if 'errors' in request.session else []
    successes = request.session['successes'] if 'successes' in request.session else []
    library = Library.objects.get(pk=library_id)
    contributed = 0
    
    if 'successes' in request.session:
        del request.session['successes']
    
    if 'errors' in request.session:
        del request.session['errors']
    
    # More efficient to do a filter() in Python? Can't page that though.
    suggestions = LibraryGame.objects.filter(
        library = library,
        suggested = True,
    ).order_by('listgame__game__name', 'listgame__game__published', 'id')
    
    games = LibraryGame.objects.filter(
        library = library,
        suggested = False,
    ).order_by('listgame__game__name', 'listgame__game__published', 'id')
    
    if request.user.is_authenticated:
        contributed = LibraryGame.objects.filter(listgame__list__user=request.user).count() > 0
    
    return {
        'successes': successes,
        'errors': errors,
        'library': library,
        'suggestions': suggestions,
        'games': games,
        'contributed': contributed,
    }

# Last so longer URLs match first.
@route(r'^library/(?P<library_id>\w+)/edit/post$')
@require_POST
def edit_post(request, library_id):
    if request.user.is_superuser or request.user.is_staff:
        errors = request.session['errors'] if 'errors' in request.session else []
        
        form = LibraryForm(request.POST, request.FILES, instance=Library.objects.get(pk=library_id))
        if form.is_valid():
            form.save()
            return redirect('boxwood.controllers.libraries.library', library_id)
        
        # save errors and redirect back to the form.
        request.session['errors'] = []
        request.session['data'] = request.POST
        request.session['files'] = request.FILES
        
        return redirect('boxwood.controllers.libraries.edit', username)
    
    raise PermissionDenied('Sorry, only staff can edit libraries.')

@route(r'^library/(?P<library_id>\w+)/edit$')
@render_html
@verified
def edit(request, library_id):
    if request.user.is_superuser or request.user.is_staff:
        
        library = Library.objects.get(pk=library_id)
        errors = request.session['errors'] if 'errors' in request.session else []
        
        if 'errors' in request.session:
            form = LibraryForm(request.session['data'], request.session['files'], instance=library)
            form.is_valid() # force validation
            del request.session['data']
            del request.session['files']
            del request.session['errors']
        else:
            form = LibraryForm(instance=library)
        
        return {
            'library': library,
            'form': form,
            'errors': errors,
        }
    raise PermissionDenied('Sorry, only staff can edit libraries.')
