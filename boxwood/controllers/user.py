from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.shortcuts import redirect
from django.db.models.functions import Lower
from django.views.decorators.http import require_POST
from boxwood.urls import route
from boxwood.models.lists import AtlasList, ListGame
from boxwood.models.users import UserForm, UserProfileForm, UserVerificationRequestForm
from boxwood.models.libraries import LibraryGame
from boxwood.jobs.lists import UpdateUserListsJob, UpdateAtlasListJob
from boxwood.views import render_html, paged, filtered, verified


@route(r'^user/(?P<username>\w+)/lists/?$')
@render_html
@paged('lists')
@verified
def lists(request, username):
    successes = request.session['successes'] if 'successes' in request.session else []
    errors = request.session['errors'] if 'errors' in request.session else []
    user = User.objects.get(username=username)
    
    # TODO remove this later when we have some sort of background sync?
    if user.profile.is_ok_to_sync():
        UpdateUserListsJob(user).now()
    
    if 'successes' in request.session:
        del request.session['successes']
    
    if 'errors' in request.session:
        del request.session['errors']
    
    return {
        'errors': errors,
        'successes': successes,
        'username': username,
        'lists': AtlasList.objects.filter(user=user).order_by(Lower('name'), 'id'),
    }

@route(r'^user/(?P<username>\w+)/(?P<list_id>\w+)/game/barcode$')
@require_POST
def set_barcode(request, username, list_id):
    if username == request.user.username:
        if 'listgame_id' in request.POST and 'barcode' in request.POST:
            listgame = ListGame.objects.get(pk=request.POST['listgame_id'])
            listgame.barcode = request.POST['barcode'].strip()
            listgame.save()
            request.session['successes'] = ['Success! Barcode added.']
            return redirect(request.POST['next'], list_id)
        return HttpResponseBadRequest(reason='Request must contain a listgame_id and a barcode parameter.')
    raise PermissionDenied('Sorry, you can only set barcodes for your own games.')

@route(r'^user/(?P<username>\w+)/list/(?P<list_id>\w+)/lend$')
@require_POST
def lend_list(request, username, list_id):
    if request.user.username == username:
        alist = AtlasList.objects.get(pk=list_id)
        alist.lendable = True
        alist.save()
        request.session['successes'] = ['Success!']
        return redirect(request.POST['next'], username, list_id)
    raise PermissionDenied('Sorry, you can only lend your own lists.')

@route(r'^user/(?P<username>\w+)/list/(?P<list_id>\w+)/unlend$')
@require_POST
def unlend_list(request, username, list_id):
    if request.user.username == username:
        alist = AtlasList.objects.get(pk=list_id)
        alist.lendable = False
        alist.save()
        LibraryGame.objects.filter(listgame__list__id=alist.id).delete()
        request.session['successes'] = ['Success!']
        return redirect(request.POST['next'], username, list_id)
    raise PermissionDenied('Sorry, you can only unlend your own lists.')

@route(r'^user/(?P<username>\w+)/list/(?P<list_id>\w+)/?$', name='boxwood.controllers.user.list')
@render_html(template='list')
@paged('games')
@filtered('games', 'game__')
@verified
def list_single(request, username, list_id):
    successes = request.session['successes'] if 'successes' in request.session else []
    errors = request.session['errors'] if 'errors' in request.session else []
    
    try:
        alist = AtlasList.objects.get(pk=list_id)
    
        if 'successes' in request.session:
            del request.session['successes']
        
        if 'errors' in request.session:
            del request.session['errors']
        
        if alist:
            # TODO remove this later when we have some sort of background sync?
            if alist.is_ok_to_sync():
                UpdateAtlasListJob(alist).now()
            
            return {
                'errors': errors,
                'successes': successes,
                'username': username,
                'list': alist,
                'games': alist.games.all().order_by(Lower('game__name'), '-game__published', 'id')
            }
    except AtlasList.DoesNotExist:
        raise Http404('Sorry, we couldn\'t find a list with that ID.')
    
    raise Exception('WTF')

# Last so longer URLs match first.
@route(r'^user/(?P<username>\w+)/edit/post$')
@require_POST
def edit_post(request, username):
    if username == request.user.username:
        # It seems from testing this doesn't save values from the client we explicitly excluded.
        # So that will protect us from people trying to change the username, etc.
        user = User.objects.get(username=username)
        user_form = UserForm(request.POST, instance=user, prefix='user')
        profile_form = UserProfileForm(request.POST, instance=user.profile, prefix='profile')
        
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return redirect('boxwood.controllers.user.profile', username)
        
        # save errors and redirect back to the form.
        request.session['errors'] = []
        request.session['data'] = request.POST
        
        return redirect('boxwood.controllers.user.edit', username)
    raise PermissionDenied('Sorry, you can only edit your own profile.')

@route(r'^user/(?P<username>\w+)/edit$')
@render_html
@verified
def edit(request, username):
    if username == request.user.username:
        
        user = User.objects.get(username=username)
        profile = user.profile
        errors = request.session['errors'] if 'errors' in request.session else []
        
        if 'errors' in request.session:
            user_form = UserForm(request.session['data'], instance=user, prefix='user')
            profile_form = UserProfileForm(request.session['data'], instance=profile, prefix='profile')
            user_form.is_valid() # force validation
            profile_form.is_valid()
            del request.session['data']
            del request.session['errors']
        else:
            user_form = UserForm(instance=user, prefix='user')
            profile_form = UserProfileForm(instance=profile, prefix='profile')
        
        return {
            'username': username,
            'user': user,
            'user_form': user_form,
            'profile_form': profile_form,
            'errors': errors,
        }
    raise PermissionDenied('Sorry, you can only edit your own profile.')

@route(r'^user/(?P<username>\w+)/verify/post$')
@require_POST
def ready_verification_post(request, username):
    # No open redirects. Prevent URLs with a full prototocol.
    go_next = request.POST['next']
    if go_next.find('://') > 0:
        return HttpResponseBadRequest(reason='Nice try.')
    
    if username == request.user.username and request.user.profile:
        
        form = UserVerificationRequestForm(request.POST, instance=request.user.profile.verification_request)
        
        if form.is_valid():
            form.save()
            return redirect(go_next)
        
        # save errors and redirect back to the form.
        request.session['errors'] = []
        request.session['data'] = request.POST
        
        return redirect('boxwood.controllers.user.edit', username)
    raise PermissionDenied('Hey, you aren\'t supposed to be here!')

@route(r'^user/(?P<username>\w+)/verify$')
@render_html
def ready_verification(request, username):
    if username == request.user.username and request.user.profile:
        profile = request.user.profile
        
        if not profile.is_verification_ready():
            errors = request.session['errors'] if 'errors' in request.session else []
            go_next =  request.session['data']['next'] if 'data' in request.session else request.GET['next']
            
            if 'errors' in request.session:
                form = UserVerificationRequestForm(request.session['data'], instance=profile.verification_request)
                form.is_valid()
                del request.session['data']
                del request.session['errors']
            else:
                form = UserVerificationRequestForm(instance=profile.verification_request)
            
            return {
                'user': request.user,
                'next': go_next,
                'form': form,
                'errors': errors
            }
        elif 'next' in request.GET:
            # No open redirects. Prevent URLs with a full prototocol.
            go_next = request.GET['next']
            if go_next.find('://') > 0:
                return HttpResponseBadRequest(reason='Nice try.')
            return redirect(go_next)
        return redirect('boxwood.controllers.user.profile', username)
    raise PermissionDenied('Hey, you aren\'t supposed to be here!')

# Last so longer URLs match first.
@route(r'^user/(?P<username>\w+)$')
@render_html
def profile(request, username):
    return {
        'username': username,
        'user': User.objects.get(username=username),
    }
