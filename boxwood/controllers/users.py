from django.contrib.auth.models import User
from django.db.models.functions import Lower
from django.core.exceptions import PermissionDenied
from django.views.decorators.http import require_POST
from django.http import HttpResponseBadRequest
from django.shortcuts import redirect
from boxwood.urls import route
from boxwood.models.lists import AtlasList
from boxwood.views import render_html, paged, verified

@route('users/requests/post')
@require_POST
def verification_requests_post(request):
    if request.user.is_staff or request.user.is_superuser:
        if 'user_id' in request.POST and 'approved' in request.POST:
            user = User.objects.get(pk=request.POST['user_id'])
            
            if request.POST['approved'] == 'true':
                user.profile.verified = True
                user.profile.save()
                UpdateUserListsJob(user).now()
            user.profile.verification_request.delete()
            
            return redirect('boxwood.controllers.users.verification_requests')
        raise HttpResponseBadRequest(reason='Missing user_id or approved parameter.')
    raise PermissionDenied('Hey! You\'re not supposed to be here.')

@route('users/requests')
@render_html
@paged('users')
@verified
def verification_requests(request):
    if request.user.is_staff or request.user.is_superuser:
        return {
            'users': User.objects.filter(profile__verification_request__isnull=False).order_by(Lower('username')),
        }
    raise PermissionDenied('Hey! You\'re not supposed to be here.')

@route('users')
@render_html
@paged('users')
@verified
def users(request):
    return {
        'users': User.objects.filter(profile__verified=True).order_by(Lower('username')),
    }

@route('lists')
@render_html
@paged('lists')
@verified
def lists(request):
    successes = request.session['successes'] if 'successes' in request.session else []
    errors = request.session['errors'] if 'errors' in request.session else []
    
    if 'successes' in request.session:
        del request.session['successes']
    
    if 'errors' in request.session:
        del request.session['errors']
    
    return {
        'errors': errors,
        'successes': successes,
        'lists': AtlasList.objects.filter(user__profile__verified=True).order_by(Lower('name'), Lower('user__username')),
    }
