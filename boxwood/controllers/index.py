from django.contrib.auth.models import User
from django.db.models.functions import Lower
from boxwood.urls import route
from boxwood.views import paged, render_html, verified
from boxwood.models import Library

@route('')
@render_html
@paged('libraries')
@verified
def index(request):
    return {
        'libraries': Library.objects.all().order_by(Lower('name')),
        'verification_requests': User.objects.filter(profile__verification_request__isnull=False).count() if request.user.is_staff or request.user.is_superuser else 0,
    }
