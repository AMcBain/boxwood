"""boxwood URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import path
from pathlib import Path
from pkgutil import walk_packages
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings
from django.conf.urls.static import static
from boxwood.views import bad_request, permission_denied, page_not_found, server_error

handler400 = bad_request
handler403 = permission_denied
handler404 = page_not_found
handler500 = server_error

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('social_django.urls', namespace='social')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

def route(arg, *args, **kwargs):
    """
    Decorator to allow functions to register themselves.
    Use as you would path() above. ex:
    
    @route('', name='index')
    def index(request):
      pass
    
    # or
    # name is assumed to be the same as the function
    @route('')
    def index(request):
      pass
    
    # Any other decorators you want applied before this one should come after it:
    @route('')
    @render_html
    def index(request):
      pass
    
    # Multiple routes can be applied to the same function if a route can't be easily described using regex.
    # In this case it is essential that both have a different name for django's benefit.
    # This matters most when doing reverse routing, such as with {% url '...' %} in templates.
    @route('/foo')
    @route('/bar', name='bar')
    @render_html
    def foo(request):
      pass
    """
    def route(func):
        if not 'name' in kwargs:
            # Module name includes 'boxwood.' at the start. We could strip that off, but this may be less confusing.
            # Especially if we actually use the ability of django to have more than one app per project. (Shocking!)
            module = func.__module__
            name = '.' + func.__name__
            
            # If the module and function name are the same, let's not repeat ourselves.
            # controllers.index.index = bleh
            if module.endswith(name):
                name = ''
            
            kwargs['name'] = module + name
        # For now; We should have a more explicit way of specifying regexes.
        if arg.startswith('^') or arg.endswith('$'):
            urlpatterns.append(re_path(arg, func, *args, **kwargs))
        else:
            urlpatterns.append(path(arg, func, *args, **kwargs))
        return func
    return route

# This is the magic. Finds all packages, modules, sub-stuff, etc. then imports it to run the decorators.
# Care must be taken not to have stuff in this package that shouldn't be auto-imported.
for finder, name, ispkg in walk_packages(path=[(Path(__file__).parent / 'controllers').absolute()], prefix='boxwood.controllers.', onerror=lambda x: None):
    __import__(name)
