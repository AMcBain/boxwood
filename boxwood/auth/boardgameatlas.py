import json
from urllib.parse import urlencode
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db import connection
from social_core.backends.oauth import BaseOAuth2
from boxwood.api.boardgameatlas import API_BASE, API_PATH
from boxwood.settings import SOCIAL_AUTH_BOARDGAMEATLAS_KEY

API_AUTH = API_BASE + 'oauth/'

class BoardGameAtlasOAuth2(BaseOAuth2):
    """Board Game Atlas OAuth authentication backend"""
    name = 'boardgameatlas'
    ID_KEY = 'id'
    AUTHORIZATION_URL = API_AUTH + 'authorize'
    ACCESS_TOKEN_URL = API_AUTH + 'token'
    ACCESS_TOKEN_METHOD = 'POST'
    REFRESH_TOKEN_URL = API_AUTH + 'token'
    REDIRECT_STATE = False
    SCOPE_SEPARATOR = ','
    EXTRA_DATA = [
        ('id', 'id'),
        ('expires', 'expires')
    ]
    
    def get_user_id(self, details, response):
        """Return a unique ID for the current user, by default from server
        response."""
        return response['user'][self.ID_KEY]
    
    def extra_data(self, user, uid, response, *args, **kwargs):
        data = super().extra_data(user, uid, response, *args, **kwargs)
        data['image'] = response['user']['images']['medium']
        data['ringCss'] = response['user']['ringCss']
        return data
    
    def get_user_details(self, response):
        """Return user details from Board Game Atlas account"""
        return {
            'id': response['user'][self.ID_KEY],
            'username': response['user']['username'],
        }
    
    def user_data(self, access_token, *args, **kwargs):
        """Loads user data from service"""
        url = API_PATH + 'user/data?' + urlencode({
            'client_id': SOCIAL_AUTH_BOARDGAMEATLAS_KEY,
            'access_token': access_token
        })
        return self.get_json(url, headers={
            'Authorization': 'Bearer ' + access_token,
        })

# This should use the ID returned in case the Atlas eventually allows changing usernames.
# Unfortunately that doesn't seem to be stored anywhere permanently. (social_auth_usersocialauth table?)
# django enforces unique usernames and the Atlas has unique usernames (their API is built around them).
# So it's not too bad to use this for now. However if we ever add more auth providers, we have to fix this.
# TODO figure out how to get the ID stored int the DB somewhere so we can use that instead.
def associate_by_id(backend, details, user=None, *args, **kwargs):
    
    if user:
        return None
    
    username = details.get('username')
    if username:
        
        user = User.objects.get(username=username)
        if user:
            return {
                'user': user,
                'is_new': False,
            }

# Because I don't know how to store this info any other way but in extra_data so we have to be able to get it back out.
# It's better this place knows than having random queries inside of some other file.
@receiver(post_save, sender=User)
def update_user_image(sender, instance, created, **kwargs):
    # We should be called after that's created but in case somehow the order of calling changes, check.
    if instance.profile:
        try:
            # FIXME replace this all with a better implementation.
            with connection.cursor() as cursor:
                cursor.execute('select extra_data from social_auth_usersocialauth where id=%s', [instance.id])
                data = json.loads(cursor.fetchone()[0])
                instance.profile.image = data['image']
                instance.profile.image_css = data['ringCss']
                instance.profile.save()
        except:
            # We should care, but it doesn't matter.
            pass

